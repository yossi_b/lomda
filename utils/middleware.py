import logging
LOGGER = logging.getLogger(__name__)

class LomdaMiddleware:
    def process_exception(self, request, exception):
        LOGGER.error('Got an exception: %s', exception)
        return None
