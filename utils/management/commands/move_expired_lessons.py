from django.conf import settings
from django.core.management import BaseCommand
from django.utils import timezone, translation

import logging
from core.models import Lesson
from utils.decorators import log_err

LOGGER = logging.getLogger(__name__)


class Command(BaseCommand):
    help = 'move lessons to new state'

    @log_err
    def handle(self, *args, **options):
        translation.activate(settings.LANGUAGE_CODE)
        self.move_from(Lesson.State.ACTIVE)
        self.move_from(Lesson.State.EVAL)

    def move_from(self,state):
        filter = dict()
        filter['{0}__lt'.format(Lesson.State.end_field[state])] = timezone.now()
        lessons = Lesson.objects.filter(state=state).filter(**filter)
        for lesson in lessons:
            LOGGER.info('lesson %s is being moved from %s to %s',lesson,lesson.get_state_display(),lesson.get_next_state_display())
            try:
                if state == Lesson.State.ACTIVE:
                    lesson.evaluate()
                elif state == Lesson.State.EVAL:
                    lesson.close()
                else:
                    assert False,'Illegal state'
            except Exception:
                LOGGER.exception('fail to move lesson %s to %s'.format(lesson,lesson.get_next_state_display()))


