# -*- coding: utf-8 -*-
from datetime import timedelta
from functools import wraps
import random

from django.core.management import BaseCommand
from django.conf import settings
from django.contrib.auth import get_user_model
from django.utils.translation import ugettext as _
from django.utils import translation, timezone
from django.core.files import File

from users.logic import create_student, create_teacher

import core.models
import student.models
import media.models
import notif.models

QUESTIONS = ['מה חושב המשורר',
                     'כמה חרוזים יש בשיר',
                     'מה מזכיר לך השיר הזה',
                     'איזה פרשה זה מזכיר',
                     'איזה אירוע היסטורי מוזכר בפרק',
                     'מה נראה לך היה הבית הבא',
                     'מה מסתתר בין השורות של השיר',
                     'האם הקטע הוא ביוגרפי',
                     'מה חושבת המשוררת לע משפחתה'
                     ]

ANSWERS = [
    'המשורר חשב שזה נכון',
    'בשיר יש חריזה מודרנית',
    'זאת שירה צעירה וחדשנית',
    'המשוררת לא אוהבת את המשפחה שלה',
    'לדעתי זה לא נכון',
    'זה לא נראה לי הגיוני',
    'יכול להיות אני לא בטוח',
    'הוא בטח התכוון למשהו אחר',
    'אין ספק שהוא היה מאוד דעתן',
    'אני חושב שזה לא הגיוני הטענה הזאת',
    'זאת שירה אמפירית לדעתי',
    'זה מאאפיין את הגל החדש של המשוררים',
    'זאת ספרות קלאסית לדעתי',
    'זה מעיד על הפרעה דו קוטבית',
    'זה נשמע לי כמו חיקוי של צכוב',
    'זה אולי מרמז על המצב האקטואלי',
    'זה אולי כן ואולי לא',
    'זה כמו טלוויזיה בשחור לבן',
    'זה לא יכול להיות - איו דברים כאלה'
]



def title(f):
    @wraps(f)
    def wrapper(*args, **kwds):
        print('Doing: {0}'.format(f.__name__))
        return f(*args, **kwds)

    return wrapper


class Command(BaseCommand):
    help = 'initialize basic testing, don\'t use in production'

    def get_user(self, abbv):
        return get_user_model().objects.get(email='{0}@lomda.com'.format(abbv))

    def get_teacher(self, abbv):
        return self.get_user(abbv).teacher

    def get_student(self, abbv):
        return self.get_user(abbv).student

    @title
    def clean(self):
        get_user_model().objects.all().delete()
        core.models.Subject.objects.all().delete()
        core.models.Course.objects.all().delete()
        core.models.Lesson.objects.all().delete()
        media.models.Media.objects.all().delete()

    @title
    def create_users(self):
        get_user_model().objects.create_superuser(email='ekeydar@gmail.com', password='Eran1234', name='ערן קידר')
        get_user_model().objects.create_superuser(email='yossi.berliner@gmail.com', password='Yossi1234', name='יוסי ברלינר')
        ORDERS = [_('zero'),
                  _('one'),
                  _('two'),
                  _('three'),
                  _('four'),
                  _('five'),
                  _('six'),
                  _('seven'),
                  _('eight'),
                  _('nine'),
                  _('ten'),
                  _('eleven'),
                  _('twelve'),
                  _('thirteen')]
        for x in range(1, 4):
            create_teacher(email='t{0}@lomda.com'.format(x),
                           name='{0} {1}'.format(_('teacher'), ORDERS[x]),
                           password='Teacher123')
        for x in range(1, 12):
            create_student(email='s{0}@lomda.com'.format(x),
                           name='{0} {1}'.format(_('student'), ORDERS[x]),
                           password='Student123')

    @title
    def create_data(self):
        sub1 = core.models.Subject.objects.create(title='ספרות')
        sub2 = core.models.Subject.objects.create(title='תנ"ך')
        t1 = self.get_teacher('t1')
        t2 = self.get_teacher('t2')

        m1 = media.models.Media()
        m1.media.save('pic1_first.png', File(open('tb/files/ken.jpg', 'br')))
        m1.save()

        course1 = core.models.Course.objects.create(subject=sub1, teacher=t1)
        course2 = core.models.Course.objects.create(subject=sub2, teacher=t2)

        for s in ['s1', 's2', 's3', 's4','s5','s6','s7']:
            course1.add_student(self.get_student(s))

        for s in ['s8', 's9', 's10', 's11']:
            course2.add_student(self.get_student(s))

        lesson1 = course1.lessons.create(title="שיעור ראשון",
                                         youtube_id='B0VQd1uXb5o',
                                         image='{0}{1}'.format(settings.HOST_URL, m1.media.url),
                                         content=open('tb/files/content1.txt',encoding='utf-8').read())

        lesson2 = course1.lessons.create(title="שיעור שני",
                                         content=open('tb/files/content2.txt',encoding='utf-8').read())

        lesson3 = course1.lessons.create(title="שיעור שלישי",
                                         content=open('tb/files/content3.txt',encoding='utf-8').read())

        lessons = [lesson1,lesson2,lesson3]

        for lidx, lesson in enumerate(lessons):
            is_last = lidx + 1 == len(lessons)
            print('updating lesson {0}'.format(lidx))
            lesson.questions.create(text='מה חשב הסופר' + '?', is_teacher=True)
            lesson.questions.create(text='למה הוא חשב את זה' + '?', is_teacher=True)
            lesson.questions.create(text='כמה זה עלה לו' + '?', is_teacher=True)
    
            lesson.activate(active_end_dt=(timezone.now() + timedelta(days=3)).replace(hour=13, minute=0,
                                                          second=0, microsecond=0),
                            eval_end_dt=(timezone.now() + timedelta(days=6)).replace(hour=13, minute=0,
                                                          second=0, microsecond=0),)

            students = ['s2','s3','s4','s5','s6','s7']
            if not is_last:
                students.append('s1')

            for idx,u in enumerate(students):
                if u != 's4':
                    record = lesson.records.get(student_record__student=self.get_student(u))
                    record.ask_question(text=QUESTIONS[idx] + '?')
    
            for idx,u in enumerate(students):
                if u not in ['s4','s5']:
                    record = lesson.records.get(student_record__student=self.get_student(u))
                    assert record.my_answers.count() == 3
                    answers_dict = {}
                    for a in record.my_answers.all():
                        answers_dict[str(a.id)] = random.choice(ANSWERS)
                    record.answer_questions(answers_dict)

            if not is_last:
                print('evaluating and closing {0}'.format(lidx))
                lesson.evaluate((timezone.now() + timedelta(days=7)).replace(hour=13, minute=0,
                                                          second=0, microsecond=0))
                for u in students:
                    if u not in ['s4','s5','s6']:
                        record = lesson.records.get(student=self.get_student(u))
                        qdict = self.build_eval_dict(record.evaluated_questions.values_list('id',flat=True))
                        adict = self.build_eval_dict(record.evaluated_answers.values_list('id',flat=True))
                        record.eval(questions=qdict,answers=adict)


                lesson.close()

    def build_eval_dict(self, oids):
        result = dict()
        for oid in oids:
            result[oid] = random.randint(5,10)
        return result

    def handle(self, *args, **options):
        assert not settings.DEPLOY_MODE, 'not in DEPLOY'
        translation.activate(settings.LANGUAGE_CODE)
        self.clean()
        self.create_users()
        self.create_data()


        # https://www.youtube.com/watch?v=vyrHd12dwjM נדנד
        # https://www.youtube.com/watch?v=B0VQd1uXb5o קן לציפור
