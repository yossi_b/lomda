import subprocess
import os

from django.core.management import BaseCommand
from django.conf import settings


class Command(BaseCommand):
    help = 'runs the msgs commands'

    def handle(self, *args, **options):
        lang = settings.LANGUAGE_CODE
        subprocess.call('python manage.py makemessages --no-location -l {0}'.format(lang), shell=True)
        file = os.path.join(settings.BASE_DIR,'locale',lang,'LC_MESSAGES/django.po')
        subprocess.call('xdg-open {0}'.format(file),shell=True)
__author__ = 'eran'
