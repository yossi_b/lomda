def avg_no_none(*scores):
    """
    :param scores: sequence of scores
    :return: averge of the non none scores, if all scores or empty- returns None
    """
    non_none = [s for s in scores if s is not None]
    if not non_none:
        return None
    return sum(non_none)/len(non_none)
