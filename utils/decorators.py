import logging
LOGGER = logging.getLogger(__name__)


def log_err(f):
    def _wrap(*args,**kwargs):
        try:
            return f(*args,**kwargs)
        except:
            LOGGER.exception('Failed in executing of %s',f.__name__)
            raise

    return _wrap



