import datetime
import json
import os
import re
import pytz
from django.conf import settings
from django.contrib.auth import get_user_model
from django.test import TestCase
import core.models
import users.logic
from student.models import CourseStudentRecord


class BaseUser:
    def __init__(self, tc):
        self.tc = tc

    def http(self, *, method, url, data, auth, expected_status, expected_error):
        method = getattr(self.tc.client, method)
        kwargs = dict()
        if auth and not self.is_anon():
            kwargs['HTTP_AUTHORIZATION'] = 'Token {0}'.format(self.user.auth_token.key)
        r = method(url, data, **kwargs)
        if r.status_code != expected_status:
            print('-------------------------')
            print(r.content.decode('utf-8'))
            print('--------------------------')
        self.tc.assertEquals(r.status_code, expected_status)
        try:
            content = json.loads(r.content.decode('utf-8'))
        except ValueError:
            content = None
        if r.status_code >= 300 and expected_error:
            self.tc.assertEquals(content, expected_error)
        return content

    def upload_file(self, fname, auth=True, expected_status=201, expected_error=None):
        f = open(os.path.join('tb/files', fname), 'rb')
        return self.post('/api/media/', {'media': f},
                         auth=auth,
                         expected_status=expected_status,
                         expected_error=expected_error)

    def post(self, url, data=None, auth=True, expected_status=201, expected_error=None):
        return self.http(method='post',
                         url=url,
                         data=data,
                         auth=auth,
                         expected_status=expected_status,
                         expected_error=expected_error)

    def patch(self, url, data, auth=True, expected_status=201, expected_error=None):
        return self.http(method='patch',
                         url=url,
                         data=data,
                         auth=auth,
                         expected_status=expected_status,
                         expected_error=expected_error)

    def get(self, url, data=None, auth=True, expected_status=200, expected_error=None):
        return self.http(method='get',
                         url=url,
                         data=data,
                         auth=auth,
                         expected_status=expected_status,
                         expected_error=expected_error)

    def create_lesson(self, subject_title, *, end_dt=None, title=None, youtube_id=None, students=[]):
        subject, _ = core.models.Subject.objects.get_or_create(title=subject_title)
        course, _ = core.models.Course.objects.get_or_create(subject=subject, teacher=self.user.teacher)
        for s in students:
            course.add_student(self.tc[s].user.student)
        lesson_idx = course.lessons.count() + 1
        data = {
            'title': title or '{0} #{1}'.format('שיעור', lesson_idx),
            'content': 'תוכן חשוב ביותר'
        }
        if youtube_id:
            data['youtube_id'] = youtube_id
        lesson_json = self.post('/api/courses/{0}/lessons/'.format(course.id), data=data)
        return lesson_json

    def create_lesson_with_questions(self, subject, **kwargs):
        lj = self.create_lesson(subject, **kwargs)
        url = lj['resource_url']
        questions = [
            'מה התכוון המשורר?',
            'למה הוא אמר את זה?',
            'מה היתה משמעות השיר?'
        ]
        for q in questions:
            self.post(url + 'questions/', data={'text': q})
        return self.get(url)


class AnonUser(BaseUser):
    def is_anon(self):
        return True


class TestUser(BaseUser):
    def __init__(self, tc, abbv, name, password):
        self.name = name
        self.abbv = abbv
        self.password = password
        super().__init__(tc)

    def is_anon(self):
        return False

    @property
    def email(self):
        return '{0}@lomda.com'.format(self.abbv)

    def create_teacher(self):
        users.logic.create_teacher(name=self.name,
                                   email=self.email,
                                   password=self.password)

    def create_student(self):
        users.logic.create_student(name=self.name,
                                   email=self.email,
                                   password=self.password)

    @property
    def user(self):
        return get_user_model().objects.get(email=self.email)

    def auth(self):
        u = self.post('/api/users/login/',
                      data={'email': self.email,
                            'password': self.password},
                      auth=False,
                      expected_status=200)
        self.tc.assertEquals(u['token'], self.user.auth_token.key)


class LomdaTestCase(TestCase):
    def __init__(self, *args, **kwargs):
        super(LomdaTestCase, self).__init__(*args, **kwargs)
        self.users = dict()
        self.users['anon'] = AnonUser(self)

    def get_fname(self):
        return '_'.join(self.id().split('.')[-2:])

    def with_ref(self):
        return self.id().endswith('_ref')

    def title(self, *args):
        print(*args)

    def parse_dt(self, t):
        from django.utils.dateparse import parse_datetime
        return parse_datetime(t)

    def assertSameTimes(self, t1, t2, msg=None):
        dt1 = self.parse_dt(t1)
        dt2 = self.parse_dt(t2)
        msg = msg or '{0} != {1}'.format(dt1.isoformat(), dt2.isoformat())
        self.assertEquals(dt1, dt2, msg)

    def assertAlmostSameTimes(self, t1, t2, delta, msg=None):
        dt1 = self.parse_dt(t1)
        dt2 = self.parse_dt(t2)
        self.assertLessEqual(abs((dt1 - dt2).total_seconds), delta)

    @property
    def log_name(self):
        return os.path.join(settings.BASE_DIR, 'tb', 'output', self.get_fname()) + '.log'

    @property
    def filtered_name(self):
        return os.path.join(settings.BASE_DIR, 'tb', 'filtered', self.get_fname()) + '.log'

    @property
    def ref_name(self):
        return os.path.join(settings.BASE_DIR, 'tb', 'ref', self.get_fname()) + '.log'

    def setUp(self):
        if not os.path.exists(os.path.dirname(self.log_name)):
            os.mkdir(os.path.dirname(self.log_name))
        if self.with_ref():
            self.log_fh = open(self.log_name, 'w')

    def tearDown(self):
        if self.with_ref():
            self.log_fh.close()
            self.assertTrue(os.path.exists(self.ref_name), '''ref file does not exists, to create an empty one do:
    touch {0}'''.format(self.ref_name))
            self.filter()
            self.do_command('diff {0} {1}'.format(self.filtered_name, self.ref_name))

    def do_command(self, cmd):
        res = os.system(cmd)
        self.assertEquals(res, 0, '''Failed in command {0}.

to rerun from shell do:
        m test {3}

To update ref run:


cp {1} {2}'''.format(cmd, self.filtered_name, self.ref_name, self.id()))

    def dump_to_log(self, out_dict, title=None):
        assert self.with_ref(), 'test name must end with _ref'
        if title:
            self.log_fh.write(title + '\n')
            self.log_fh.write('-----------------------------------------------------\n')
        self.log_fh.write(json.dumps(out_dict, indent=4, sort_keys=True))
        self.log_fh.write('\n\n\n')

    def filter(self):
        if not os.path.exists(os.path.dirname(self.filtered_name)):
            os.mkdir(os.path.dirname(self.filtered_name))
        rh = open(self.log_name)
        wh = open(self.filtered_name, 'w')
        for line in rh:
            line = re.sub('"start_dt": ".*"', '"start_dt": "<DATE>"', line)
            line = re.sub(r'"image": "http://(.*)/uploads/pic1_(.*).jpg"',
                          r'"image": "http:/<HOST>/uploads/pic1_<PIC>.jpg"',
                          line)
            line = re.sub(r'"(?P<field>created_at|submitted_at|active_at|eval_at|end_dt|active_end_dt|eval_end_dt)"'
                          ': "\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}Z"',
                          r'"\1": "<DATE_TIME>"',
                          line)

            wh.write(line)
        wh.close()
        rh.close()

    @property
    def now_il(self):
        return pytz.timezone('Asia/Jerusalem').localize(datetime.datetime.now()).replace(microsecond=0)

    def days_ahead(self, days=4):
        return (self.now_il + datetime.timedelta(days=days)).replace(hour=15, minute=0, second=0).isoformat()

    def days_ago(self, days=4):
        return (self.now_il - datetime.timedelta(days=days)).replace(hour=15, minute=0, second=0).isoformat()

    @property
    def tomorrow(self):
        return self.days_ahead(1)

    @property
    def next_week(self):
        return self.days_ahead(8)

    def add_users(self, tc, uc):
        for x in range(1, tc + 1):
            self.add_teacher('מורה {0}'.format(x), 't{0}'.format(x), auth=True)
        for x in range(1, uc + 1):
            self.add_student('תלמיד {0}'.format(x), 's{0}'.format(x), auth=True)

    def add_teacher(self, name, abbv, auth=False):
        self.assertNotIn(abbv, self.users, msg='abbv must be unique')
        password = 'Teacher1234'
        u = TestUser(self, abbv, name, password)
        self.users[abbv] = u
        u.create_teacher()
        if auth:
            self[abbv].auth()

    def add_student(self, name, abbv, auth=False):
        self.assertNotIn(abbv, self.users, msg='abbv must be unique')
        password = 'Student1234'
        u = TestUser(self, abbv, name, password)
        self.users[abbv] = u
        u.create_student()
        if auth:
            self[abbv].auth()

    def add_students_to_course(self, course_id, *abbvs):
        course = core.models.Course.objects.get(pk=course_id)
        for abbv in abbvs:
            student = self[abbv].user.student
            sr = CourseStudentRecord(student=student,
                                     course=course)
            sr.full_clean()
            sr.save()

    def format_utc(self, dt):
        return dt.astimezone(pytz.utc).isoformat().replace('+00:00', 'Z')

    def ids(self, *abbvs):
        return [self[abbv].user.id for abbv in abbvs]

    def __getitem__(self, abbv):
        return self.users[abbv]
