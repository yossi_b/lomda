from django.conf import settings

from .tb_utils import LomdaTestCase


class TestUploadMedia(LomdaTestCase):
    def test1(self):
        self.add_teacher('מורה אחד','t1',auth=True)
        medias = []
        for x in range(4):
            medias.append(self['t1'].upload_file('pic1.jpg'))
        for i in range(1,len(medias)):
            self.assertNotEqual(medias[i]['media'],medias[i-1]['media'])
        for m in medias:
            exp_prefix = '{0}/uploads/pic1_'.format(settings.HOST_URL)
            self.assertTrue(m['media'].startswith(exp_prefix))
            self.assertTrue(m['media'].endswith('.jpg'))