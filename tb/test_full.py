import json

import core.models
from .tb_utils import LomdaTestCase


class TestFullTestCase(LomdaTestCase):
    def make_full_lesson(self):
        self.add_users(2, 10)
        students = ['s1', 's2', 's3', 's4', 's5']
        lj = self['t1'].create_lesson('שירה', title='שיעור שני', students=students)
        url = lj['resource_url']
        self.assertEquals(lj['title'], 'שיעור שני')
        self['t1'].post(url + 'edit/', data={'title': 'שיעור ראשון'}, expected_status=200)
        lj = self['t1'].post(url + 'edit/', data={'youtube_id': 'vyrHd12dwjM'}, expected_status=200)

        self.assertEquals(lj['title'], 'שיעור ראשון')
        self.assertEquals(lj['state'], 'initial')
        self.assertEquals(lj['next_state'], 'active')

        self.title('add 3 questions')
        self['t1'].post(url + 'questions/', data=dict(text='שאלה ראשונה מורה'))
        self['t1'].post(url + 'questions/', data=dict(text='שאלה שניה מורה'))
        self['t1'].post(url + 'questions/', data=dict(text='שאלה שלישית מורה'))

        for s in students:
            u_course = self[s].get('/api/courses/current/')
            self.assertEquals(u_course['subject']['title'], 'שירה')
            self.assertIsNone(u_course['current_lesson'])

        lj = self['t1'].post(url + 'activate/',
                             data={
                                 'active_end_dt': self.days_ahead(2),
                                 'eval_end_dt': self.days_ahead(3)
                             }, expected_status=200)
        self.assertEquals(lj['title'], 'שיעור ראשון')
        self.assertEquals(lj['state'], 'active')
        self.assertEquals(lj['next_state'], 'eval')

        for s in students:
            self.ask_and_answer(s, lj)

        lj = self['t1'].post(url + 'evaluate/', expected_status=200)
        self.assertEquals(lj['title'], 'שיעור ראשון')
        self.assertEquals(lj['state'], 'eval')
        self.assertEquals(lj['next_state'], 'close')

        for s in students:
            self.eval(s, lj)

        self.title('verify scores are hidden for now')
        for s in students:
            sr = self[s].get(lj['resource_url'] + 'student-records/mine/')
            self.assertIsNone(sr['qa_score'])
            self.assertIsNone(sr['eqa_score'])

        lj = self['t1'].post(url + 'close/', expected_status=200)
        self.assertEquals(lj['title'], 'שיעור ראשון')
        self.assertEquals(lj['state'], 'close')
        self.assertIsNone(lj['next_state'])

        for s in students:
            sr = self[s].get(lj['resource_url'] + 'student-records/mine/')
            self.assertIsNotNone(sr['qa_score'])
            self.assertGreater(sr['qa_score'],1.2)
            self.assertIsNotNone(sr['eqa_score'])
            self.assertGreater(sr['eqa_score'],1.2)

    def ask_and_answer(self, s, lj):
        u_course = self[s].get('/api/courses/current/')
        self.assertEquals(u_course['subject']['title'], 'שירה')
        self.assertEqual(u_course['current_lesson']['id'], lj['id'])
        lurl = u_course['current_lesson']['resource_url']
        sr = self[s].get(lurl + 'student-records/mine/')
        self.assertEquals(sr['state'], 'ask')
        self.assertEquals(sr['lesson']['resource_url'], lurl)
        sr_url = sr['resource_url']
        self.title(self[s].user.email + ' asking')
        sr = self[s].post(sr_url + 'ask-question/', data=dict(text='שאלה של {0}'.format(self[s].user.name)),
                          expected_status=200)
        self.assertEquals(sr['state'], 'answer')
        answers_dict = dict()
        self.assertEquals(sr['evaluated_answers'], [])
        self.assertEquals(sr['evaluated_questions'], [])
        for a in sr['answers']:
            answers_dict[a['id']] = 'תשובה של {0}'.format(self[s].user.name)
        sr = self[s].post(sr_url + 'answer-questions/', data=dict(answers=json.dumps(answers_dict)),
                          expected_status=200)
        self.assertEquals(sr['state'], 'eval')
        self.assertEquals(sr['lesson']['state'], 'active')
        self.assertEquals(sr['evaluated_answers'], [])
        self.assertEquals(sr['evaluated_questions'], [])

    def eval(self, s, lj):
        u_course = self[s].get('/api/courses/current/')
        self.assertEquals(u_course['subject']['title'], 'שירה')
        self.assertEqual(u_course['current_lesson']['id'], lj['id'])
        lurl = u_course['current_lesson']['resource_url']
        sr = self[s].get(lurl + 'student-records/mine/')
        self.assertEquals(sr['state'], 'eval')
        self.assertEquals(sr['lesson']['state'], 'eval')
        self.assertGreaterEqual(len(sr['evaluated_answers']), 3)
        self.assertEquals(len(sr['evaluated_questions']), 3)
        sr_url = sr['resource_url']
        ea_dict = {}
        eq_dict = {}
        for ea in sr['evaluated_answers']:
            ea_dict[ea['id']] = 3 + (7*self[s].user.id + len(ea['text'])) % 8
        for eq in sr['evaluated_questions']:
            eq_dict[eq['id']] = 4 + (7*self[s].user.id + len(ea['text'])) % 7
        self[s].post(sr_url + 'eval/', data=dict(answers=json.dumps(ea_dict),
                                                 questions=json.dumps(eq_dict)),
                     expected_status=200)


    def test_full(self):
        self.make_full_lesson()

    def test_full_student_view_ref(self):
        s = 's1'
        self.make_full_lesson()
        course_resp = self[s].get('/api/courses/current/')
        self.dump_to_log(course_resp,'current course response for student')
        self.assertEquals(course_resp['current_lesson'],None)
        course_url = course_resp['resource_url']
        self[s].get(course_url + 'lessons/',expected_status=403)
        summary = self[s].get(course_url + 'student-records/mine/summary/')
        self.assertEquals(summary['class_qa_score'],core.models.Course.objects.get(pk=course_resp['id']).class_qa_score)
        self.dump_to_log(summary,'Student Course Summary')
        self.title('try to get other student records')
        self[s].get(course_url + 'student-records/3/',expected_status=403)
        self[s].get(course_url + 'student-records/2/summary/',expected_status=404)
        cr = self[s].get(course_url + 'student-records/mine/')
        cr_obj = self[s].user.student.course_records.all().get()
        self.assertEquals(cr['id'],cr_obj.id)
        self.dump_to_log(cr,'Student Course Record')
        self.title('get lesson record')
        self.assertEquals(len(cr['lesson_records']),1)
        lr = cr['lesson_records'][0]
        self.assertEquals(lr['state'],'done')
        self.assertEquals(lr['lesson']['state'],'close')
        lesson_url = lr['lesson']['resource_url']
        self[s].get(lesson_url,expected_status=403)
        my_lr = self[s].get(lesson_url + 'student-records/mine/')
        self.dump_to_log(my_lr,'My Lesson Record')
        self[s].get(lesson_url + 'student-records/2/',expected_status=403)
        self.title('Now get the questions')
        questions = self[s].get(lesson_url + 'questions/student-view/')
        self.dump_to_log(questions,'Questions')
        lr_obj = cr_obj.lesson_records.get(lesson=lr['lesson']['id'])
        self.assertEquals(lr_obj.id,lr['id'])
        my_qids = list(lr_obj.my_questions.values_list('id',flat=True))
        self.assertEquals(len(my_qids),1)
        my_aids = list(lr_obj.my_answers.values_list('id',flat=True))
        for q in questions:
            if q['id'] in my_qids:
                self.assertTrue(q['mine'])
            else:
                self.assertFalse(q['mine'])
            self.assertIn('score',q)
        for q in questions:
            for a in q['answers']:
                if a['id'] in my_aids:
                    self.assertTrue(a['mine'])
                else:
                    self.assertFalse(a['mine'])
            self.assertIn('score',a)

    def test_full_teacher_view_ref(self):
        self.make_full_lesson()
        course_resp = self['t1'].get('/api/courses/current/')
        self.dump_to_log(course_resp,'current course response for student')
        self.assertEquals(course_resp['current_lesson'],None)
        course_url = course_resp['resource_url']
        lessons = self['t1'].get(course_url + 'lessons/')
        self.assertEquals(len(lessons),1)
        self.dump_to_log(lessons,title='all lessons')
        self['s2'].get(course_url + 'lessons/',expected_status=403)
        lesson = self['t1'].get(lessons[0]['resource_url'])
        self.dump_to_log(lesson,title='lesson')
        questions = self['t1'].get(lessons[0]['resource_url'] + 'questions/')
        self.dump_to_log(questions,title='questions')
        self['s1'].get(lessons[0]['resource_url'] + 'questions/',expected_status=403)
        records = self['t1'].get(lessons[0]['resource_url'] + 'student-records/teacher/')
        self.dump_to_log(records,title='student records')
        self['s1'].get(lessons[0]['resource_url'] + 'student-records/teacher/',expected_status=403)
        full_questions = self['t1'].get(lessons[0]['resource_url'] + 'questions/full/')
        self.dump_to_log(full_questions,title='full questions')





