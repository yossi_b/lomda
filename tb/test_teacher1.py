from django.utils import timezone
from django.utils.dateparse import parse_datetime

import core.models
from .tb_utils import LomdaTestCase


class GetLessonTestCase(LomdaTestCase):
    def test_get_lesson_ref(self):
        self.add_teacher('מורה אחד', 't1')
        subject = core.models.Subject.objects.create(title='ספרות')
        course = core.models.Course.objects.create(subject=subject,
                                                   teacher=self['t1'].user.teacher)
        lesson = core.models.Lesson.objects.create(course=course,
                                                   title='שיעור ראשון',
                                                   youtube_id='qx4v4tCZMp8',
                                                   content='תוכן רציני מאוד')
        self['t1'].auth()
        lesson_json = self['t1'].get('/api/courses/{0}/lessons/{1}/'.format(lesson.course_id, lesson.id))
        self.dump_to_log(lesson_json)

    def test_get_other_lesson(self):
        self.add_teacher('מורה אחד', 't1')
        self.add_teacher('מורה שתיים', 't2')
        subject = core.models.Subject.objects.create(title='ספרות')
        course = core.models.Course.objects.create(subject=subject,
                                                   teacher=self['t1'].user.teacher)
        lesson = core.models.Lesson.objects.create(course=course,
                                                   youtube_id='qx4v4tCZMp8')
        self['t2'].auth()
        self['t2'].get('/api/courses/{0}/lessons/{1}/'.format(lesson.course.id, lesson.id), expected_status=403)

        self['anon'].get('/api/courses/{0}/lessons/{1}/'.format(lesson.course.id, lesson.id), expected_status=401)


class CreateLessonTestCase(LomdaTestCase):
    def test_ok_ref(self):
        self.add_teacher('מורה אחד', 't1')
        core.models.Subject.objects.create(title='ספרות')
        subject = core.models.Subject.objects.create(title='שירה')
        course = core.models.Course.objects.create(subject=subject,
                                                   teacher=self['t1'].user.teacher)
        self['t1'].auth()
        data = {
            'title': 'שיעור לדוגמא',
            'content': 'תוכן רציני מאוד'
        }
        lesson_json = self['t1'].post('/api/courses/{0}/lessons/'.format(course.id), data=data, expected_status=201)
        self.dump_to_log(lesson_json)

    def test_ok_with_image_ref(self):
        self.add_teacher('מורה אחד', 't1', auth=True)
        core.models.Subject.objects.create(title='ספרות')
        subject = core.models.Subject.objects.create(title='שירה')
        course = core.models.Course.objects.create(subject=subject,
                                                   teacher=self['t1'].user.teacher)
        media = self['t1'].upload_file('pic1.jpg')
        self['t1'].auth()
        data = {
            'title': 'שיעור שלי',
            'image': media['media'],
            'content': 'תוכן רציני מאוד'
        }
        lesson_json = self['t1'].post('/api/courses/{0}/lessons/'.format(course.id), data=data, expected_status=201)
        self.dump_to_log(lesson_json)

    def test_other_course(self):
        self.add_teacher('מורה אחד', 't1')
        self.add_teacher('מורה שתיים', 't2')
        core.models.Subject.objects.create(title='ספרות')
        subject = core.models.Subject.objects.create(title='שירה')
        course1 = core.models.Course.objects.create(subject=subject,
                                                    teacher=self['t1'].user.teacher)
        course2 = core.models.Course.objects.create(subject=subject,
                                                    teacher=self['t2'].user.teacher)
        self['t1'].auth()
        self['t2'].auth()
        data = {
            'title': 'אסור לי',
            'content': 'תוכן רציני מאוד'
        }
        self['t2'].post('/api/courses/{0}/lessons/'.format(course1.id), data=data, expected_status=403)

    def test_student(self):
        self.add_teacher('מורה אחד', 't1', auth=True)
        self.add_student('תלמידון', 's1', auth=True)
        core.models.Subject.objects.create(title='ספרות')
        subject = core.models.Subject.objects.create(title='שירה')
        course = core.models.Course.objects.create(subject=subject,
                                                   teacher=self['t1'].user.teacher)
        data = {
            'content': 'תוכן רציני מאוד'
        }
        r = self['s1'].post('/api/courses/{0}/lessons/'.format(course.id), data=data, expected_status=403)

    def test_error_title(self):
        self.add_teacher('מורה אחד', 't1', auth=True)
        ls1 = self['t1'].create_lesson('ספרות', title='שיעור ראשון')
        course_id = ls1['course']['id']
        data = {
            'title': 'שיעור ראשון',
            'content': 'תוכן רציני מאוד'
        }
        self['t1'].post('/api/courses/{0}/lessons/'.format(course_id), data=data,
                        expected_status=400,
                        expected_error={'title': ['יש כבר שיעור עם כותרת זאת']})


class EditLesson(LomdaTestCase):
    def test_edit_title(self):
        self.add_teacher('מורה אחד', 't1', auth=True)
        subject = core.models.Subject.objects.create(title='שירה')
        course = core.models.Course.objects.create(subject=subject,
                                                   teacher=self['t1'].user.teacher)
        data = {
            'title': 'שיעור לדוגמא',
            'content': 'תוכן רציני מאוד'
        }
        lj1 = self['t1'].post('/api/courses/{0}/lessons/'.format(course.id), data=data)
        self.assertEquals(lj1['title'], 'שיעור לדוגמא')
        edit_url = '/api/courses/{0}/lessons/{1}/edit/'.format(course.id, lj1['id'])
        lj2 = self['t1'].post(edit_url, data={'title': 'כותרת חדשה'}, expected_status=200)
        self.assertEquals(lj2['title'], 'כותרת חדשה')
        del lj1['title']
        del lj2['title']
        self.assertEquals(lj1, lj2)

    def test_edit_title_no_change(self):
        self.add_teacher('מורה אחד', 't1', auth=True)
        subject = core.models.Subject.objects.create(title='שירה')
        course = core.models.Course.objects.create(subject=subject,
                                                   teacher=self['t1'].user.teacher)
        data = {
            'title': 'שיעור לדוגמא',
            'content': 'תוכן רציני מאוד'
        }
        lj1 = self['t1'].post('/api/courses/{0}/lessons/'.format(course.id), data=data)
        self.assertEquals(lj1['title'], 'שיעור לדוגמא')
        edit_url = '/api/courses/{0}/lessons/{1}/edit/'.format(course.id, lj1['id'])
        lj2 = self['t1'].post(edit_url, data={'title': 'שיעור לדוגמא'}, expected_status=200)
        self.assertEquals(lj1, lj2)

    def test_edit_title_not_unique(self):
        self.add_teacher('מורה אחד', 't1', auth=True)
        lj1 = self['t1'].create_lesson('שירה', title='שיעור ראשון')
        lj2 = self['t1'].create_lesson('שירה', title='שיעור שני')
        edit_url = lj2['resource_url'] + 'edit/'
        self['t1'].post(edit_url, data={'title': 'שיעור ראשון'},
                        expected_status=400,
                        expected_error={'title': ['יש כבר שיעור עם כותרת זאת']})

    def test_edit_title_lesson_closed(self):
        self.add_teacher('מורה אחד', 't1', auth=True)
        lj1 = self['t1'].create_lesson('שירה', title='שיעור ראשון')
        self['t1'].post(lj1['resource_url'] + 'questions/', data={'text': 'שאלה 1'})
        self['t1'].post(lj1['resource_url'] + 'questions/', data={'text': 'שאלה 2'})
        self['t1'].post(lj1['resource_url'] + 'questions/', data={'text': 'שאלה 3'})
        self['t1'].post(lj1['resource_url'] + 'activate/',
                        data={
                            'active_end_dt': self.days_ahead(2),
                            'eval_end_dt': self.days_ahead(4)
                        },
                        expected_status=200)
        self['t1'].post(lj1['resource_url'] + 'evaluate/', data={'end_dt': self.days_ahead(3)},
                        expected_status=200)
        self['t1'].post(lj1['resource_url'] + 'close/', data={'next_state': 'close'}, expected_status=200)
        self['t1'].post(lj1['resource_url'] + 'edit/', data={'title': 'שיעור ראשון'},
                        expected_status=400,
                        expected_error={'non_field_errors': 'השיעור סגור'})

    def test_edit_content(self):
        self.add_teacher('מורה אחד', 't1', auth=True)
        subject = core.models.Subject.objects.create(title='שירה')
        course = core.models.Course.objects.create(subject=subject,
                                                   teacher=self['t1'].user.teacher)
        data = {
            'title': 'שיעור לדוגמא',
            'content': 'תוכן רציני מאוד'
        }
        lj1 = self['t1'].post('/api/courses/{0}/lessons/'.format(course.id), data=data)
        self.assertEquals(lj1['title'], 'שיעור לדוגמא')
        edit_url = '/api/courses/{0}/lessons/{1}/edit/'.format(course.id, lj1['id'])
        lj2 = self['t1'].post(edit_url, data={'content': lj1['content'] + '\nועוד שורה חדשה'}, expected_status=200)
        self.assertEquals(lj2['content'], lj1['content'] + '\nועוד שורה חדשה')
        lj3 = self['t1'].get('/api/courses/{0}/lessons/{1}/'.format(course.id, lj1['id']))
        self.assertEquals(lj2, lj3)
        del lj1['content']
        del lj2['content']
        self.assertEquals(lj1, lj2)

    def test_delete_field_image_ref(self):
        self.add_teacher('מורה אחד', 't1', auth=True)
        core.models.Subject.objects.create(title='ספרות')
        subject = core.models.Subject.objects.create(title='שירה')
        course = core.models.Course.objects.create(subject=subject,
                                                   teacher=self['t1'].user.teacher)
        data = {
            'title': 'שיעור שלי',
            'image': 'http://localhost:8000/uploads/pic1_abcdef.jpg',
            'content': 'תוכן רציני מאוד',
        }
        lesson_json = self['t1'].post('/api/courses/{0}/lessons/'.format(course.id), data=data, expected_status=201)

        self.dump_to_log(lesson_json)
        lj2 = self['t1'].post('/api/courses/{0}/lessons/{1}/delete-field/'.format(course.id, lesson_json['id']),
                              data={'image': '-----'},
                              expected_status=200)
        self.assertIsNone(lj2['image'], None)
        del lj2['image']
        del lesson_json['image']
        self.assertEquals(lj2, lesson_json)

    def test_delete_field_youtube_id(self):
        self.add_teacher('מורה אחד', 't1', auth=True)
        lj1 = self['t1'].create_lesson('ספרות', youtube_id='vyrHd12dwjM')
        self.assertEquals(lj1['youtube_id'], 'vyrHd12dwjM')
        lj2 = self['t1'].post('/api/courses/{0}/lessons/{1}/delete-field/'.format(lj1['course']['id'], lj1['id']),
                              data={'youtube_id': '-----'},
                              expected_status=200)
        self.assertIsNone(lj2['youtube_id'])
        del lj1['youtube_id']
        del lj2['youtube_id']
        self.assertEquals(lj2, lj1)

    def test_delete_field_title(self):
        self.add_teacher('מורה אחד', 't1', auth=True)
        lj1 = self['t1'].create_lesson('ספרות', youtube_id='vyrHd12dwjM')
        self.assertEquals(lj1['youtube_id'], 'vyrHd12dwjM')
        lj2 = self['t1'].post('/api/courses/{0}/lessons/{1}/delete-field/'.format(lj1['course']['id'], lj1['id']),
                              data={'title': '-----'},
                              expected_status=200)
        self.assertIsNotNone(lj2['title'])
        self.assertEquals(lj2, lj1)


class CreateQuestions(LomdaTestCase):
    def test_ok(self):
        self.add_teacher('מורה אחד', 't1')
        self['t1'].auth()
        lesson = self['t1'].create_lesson('ספרות')
        r = self['t1'].post('/api/courses/{0}/lessons/{1}/questions/'.format(lesson['course']['id'],
                                                                             lesson['id']),
                            data={'text': 'מה התכוון המשורר'})

    def test_unique_name(self):
        self.add_teacher('מורה אחד', 't1')
        self['t1'].auth()
        lesson = self['t1'].create_lesson('ספרות')
        self['t1'].post('/api/courses/{0}/lessons/{1}/questions/'.format(lesson['course']['id'],
                                                                         lesson['id']),
                        data={'text': 'מה התכוון המשורר'})
        self['t1'].post('/api/courses/{0}/lessons/{1}/questions/'.format(lesson['course']['id'],
                                                                         lesson['id']),
                        data={'text': 'מה התכוון המשורר'},
                        expected_status=400,
                        expected_error={'text': ['יש כבר שאלה כזאת']}
                        )

    def test_mismatch_lesson_course(self):
        self.add_teacher('מורה אחד', 't1')
        self['t1'].auth()
        lesson1 = self['t1'].create_lesson('ספרות')
        lesson2 = self['t1'].create_lesson('שירה')
        r = self['t1'].post('/api/courses/{0}/lessons/{1}/questions/'.format(lesson1['course']['id'],
                                                                             lesson2['id']),
                            data={'text': 'מה התכוון המשורר'},
                            expected_status=404)

    def add_question_closed_lesson(self):
        self.add_teacher('מורה אחד', 't1')
        self['t1'].auth()
        lesson = self['t1'].create_lesson('ספרות')

        r = self['t1'].post('/api/courses/{0}/lessons/{1}/questions/'.format(lesson['course']['id'],
                                                                             lesson['id']),
                            data={'text': 'מה התכוון המשורר'},
                            expected_status=400,
                            expected_error={'details': 'השיעור סגור'})


class EditQuestion(LomdaTestCase):
    def test_ok(self):
        self.add_teacher('מורה אחד', 't1')
        self['t1'].auth()
        lesson = self['t1'].create_lesson('ספרות')
        lesson_url = '/api/courses/{0}/lessons/{1}/'.format(lesson['course']['id'], lesson['id'])
        q1 = self['t1'].post('{0}questions/'.format(lesson_url),
                             data={'text': 'מה התכוון המשורר'})
        q2 = self['t1'].post('{0}questions/'.format(lesson_url),
                             data={'text': 'העם השיר יפה?'})
        q3 = self['t1'].post('{0}questions/'.format(lesson_url),
                             data={'text': 'מה אתם חושבים?'})
        questions = self['t1'].get('{0}questions/'.format(lesson_url))
        self.assertEquals(len(questions), 3)
        self.assertEquals(questions[0]['text'], 'מה התכוון המשורר')
        self.assertEquals(questions[1]['text'], 'העם השיר יפה?')
        self.assertEquals(questions[2]['text'], 'מה אתם חושבים?')
        for q in questions:
            self.assertIsNone(q['last_edited_at'])
        self['t1'].post('{0}questions/{1}/edit/'.format(lesson_url, q2['id']), data={'text': 'האם השיר יפה?'},
                        expected_status=200)
        questions = self['t1'].get('{0}questions/'.format(lesson_url))
        self.assertEquals(len(questions), 3)
        self.assertEquals(questions[0]['text'], 'מה התכוון המשורר')
        self.assertEquals(questions[1]['text'], 'האם השיר יפה?')
        self.assertIsNotNone(questions[1]['last_edited_at'])
        edited_at = parse_datetime(questions[1]['last_edited_at'])
        self.assertLessEqual((timezone.now() - edited_at).total_seconds(), 0.3)
        self.assertEquals(questions[2]['text'], 'מה אתם חושבים?')

    def test_unique_name(self):
        self.add_teacher('מורה אחד', 't1', auth=True)
        lesson = self['t1'].create_lesson('ספרות')
        lesson_url = '/api/courses/{0}/lessons/{1}/'.format(lesson['course']['id'], lesson['id'])
        q1 = self['t1'].post('{0}questions/'.format(lesson_url),
                             data={'text': 'מה התכוון המשורר'})
        q2 = self['t1'].post('{0}questions/'.format(lesson_url),
                             data={'text': 'העם השיר יפה?'})
        q3 = self['t1'].post('{0}questions/'.format(lesson_url),
                             data={'text': 'מה אתם חושבים?'})
        self['t1'].post('{0}questions/{1}/edit/'.format(lesson_url, q2['id']), data={'text': 'מה התכוון המשורר'},
                        expected_status=400,
                        expected_error={"text": ["יש כבר שאלה כזאת"]})

    def test_fields(self):
        self.add_teacher('מורה אחד', 't1', auth=True)
        lesson = self['t1'].create_lesson('ספרות')
        lesson_url = '/api/courses/{0}/lessons/{1}/'.format(lesson['course']['id'], lesson['id'])
        q1 = self['t1'].post('{0}questions/'.format(lesson_url),
                             data={'text': 'מה התכוון המשורר'})
        q2 = self['t1'].post('{0}questions/'.format(lesson_url),
                             data={'text': 'העם השיר יפה?'})
        q3 = self['t1'].post('{0}questions/'.format(lesson_url),
                             data={'text': 'מה אתם חושבים?'})
        resp1 = self['t1'].post('{0}questions/{1}/edit/'.format(lesson_url, q2['id']), data={'text': 'שלאה חדשה',
                                                                                             'created_at': self.days_ago(
                                                                                                 10)},
                                expected_status=200)
        self.assertEquals(resp1['created_at'], q2['created_at'])
        r = self['t1'].post('{0}questions/{1}/edit/'.format(lesson_url, q2['id']),
                            data={'created_at2': self.days_ago(10)},
                            expected_status=400,
                            expected_error={'text': ['יש להזין תוכן בשדה זה.']})

    def test_edit_404(self):
        self.add_teacher('מורה אחד', 't1', auth=True)
        self.add_teacher('מורה שני', 't2', auth=True)
        lesson1 = self['t1'].create_lesson('ספרות')
        lesson2 = self['t2'].create_lesson('אנגלית')
        lesson1_url = '/api/courses/{0}/lessons/{1}/'.format(lesson1['course']['id'], lesson1['id'])
        lesson2_url = '/api/courses/{0}/lessons/{1}/'.format(lesson2['course']['id'], lesson2['id'])
        q1 = self['t1'].post('{0}questions/'.format(lesson1_url),
                             data={'text': 'מה התכוון המשורר'})
        self['t1'].post('{0}questions/'.format(lesson2_url),
                        data={'text': 'מה התכוון המשורר'}, expected_status=403)
        q2 = self['t2'].post('{0}questions/'.format(lesson2_url),
                             data={'text': 'מה התכוון המשורר'})

        self['t1'].post('{0}questions/{1}/edit/'.format(lesson2_url, q1['id']), data={'text': 'האם השיר יפה?'},
                        expected_status=403)
        self['t1'].post('/api/courses/{0}/lessons/{1}/questions/{2}/edit/'.format(
            lesson1['course']['id'],
            lesson2['id'],
            q1['id']), data={'text': 'האם השיר יפה?'}, expected_status=404)

        self['t1'].post('/api/courses/{0}/lessons/{1}/questions/{2}/edit/'.format(
            lesson1['course']['id'],
            lesson1['id'],
            q1['id']), data={'text': 'האם השיר יפה?'}, expected_status=200)

        self['t1'].post('/api/courses/{0}/lessons/{1}/questions/{2}/edit/'.format(
            lesson1['course']['id'] + 100,
            lesson1['id'],
            q1['id']), data={'text': 'האם השיר יפה?'}, expected_status=404)

        self['t1'].post('/api/courses/{0}/lessons/{1}/questions/{2}/edit/'.format(
            lesson1['course']['id'],
            lesson1['id'] + 100,
            q1['id']), data={'text': 'האם השיר יפה?'}, expected_status=404)

        self['t1'].post('/api/courses/{0}/lessons/{1}/questions/{2}/edit/'.format(
            lesson1['course']['id'],
            lesson1['id'],
            q1['id'] + 100), data={'text': 'האם השיר יפה?'}, expected_status=404)


class CourseRecords(LomdaTestCase):
    def test_simple1(self):
        self.add_teacher('מורה אחד', 't1', auth=True)
        self.add_student('תלמיד אחד', 's1', auth=True)
        self.add_student('תלמיד שתים', 's2', auth=True)
        self.add_student('תלמיד שלוש', 's3', auth=True)
        self.add_student('תלמיד ארבע', 's4', auth=True)
        lesson = self['t1'].create_lesson('ספרות')
        self.add_students_to_course(lesson['course']['id'], 's1', 's2', 's3', 's4')
        course = self['t1'].get('/api/courses/current/')
        self.assertEquals(course['subject']['title'], 'ספרות')
        self.assertEquals(course['id'], lesson['course']['id'])
        records = self['t1'].get('/api/courses/{0}/student-records/'.format(course['id']))
        self.assertEquals(len(records), 4)
        self.assertSequenceEqual([r['student']['id'] for r in records],
                                 self.ids('s1', 's2', 's3', 's4'))

    def test_not_permitted(self):
        self.add_teacher('מורה אחד', 't1', auth=True)
        self.add_teacher('מורה אחד', 't2', auth=True)
        self.add_student('תלמיד אחד', 's1', auth=True)
        self.add_student('תלמיד שתים', 's2', auth=True)
        self.add_student('תלמיד שלוש', 's3', auth=True)
        self.add_student('תלמיד ארבע', 's4', auth=True)
        lesson = self['t1'].create_lesson('ספרות')
        self.add_students_to_course(lesson['course']['id'], 's1', 's2', 's3', 's4')
        course = self['t1'].get('/api/courses/current/')
        self.assertEquals(course['subject']['title'], 'ספרות')
        self.assertEquals(course['id'], lesson['course']['id'])
        self['t2'].get('/api/courses/{0}/student-records/'.format(course['id']), expected_status=403)
        self['anon'].get('/api/courses/{0}/student-records/'.format(course['id']), expected_status=401)
        self['s1'].get('/api/courses/{0}/student-records/'.format(course['id']), expected_status=403)


class LessonStates(LomdaTestCase):
    def test_3q_activate(self):
        self.add_teacher('מורה אחד', 't1', auth=True)
        lj1 = self['t1'].create_lesson('ספרות', title='שיעור ראשון')
        lj = self['t1'].get(lj1['resource_url'])
        self.assertEquals(lj, lj1)
        url = lj['resource_url']
        self.assertEquals(lj['state'], 'initial')
        self.assertIsNone(lj['active_end_dt'])
        self.assertIsNone(lj['eval_end_dt'])
        self['t1'].post(url + 'activate/',
                        data={'active_end_dt': self.days_ahead(3), 'eval_end_dt': self.days_ahead(6)},
                        expected_status=400,
                        expected_error={"non_field_errors": "דרושות שלוש שאלות ע\"מ להפעיל שיעור"})

    def test_ok(self):
        self.add_users(1, 5)
        lj = self['t1'].create_lesson_with_questions('ספרות', title='שיעור ראשון')
        url = lj['resource_url']
        lj = self['t1'].post(url + 'activate/',
                             data={'active_end_dt': self.days_ahead(3),
                                   'eval_end_dt': self.days_ahead(6)},
                             expected_status=200)
        self.assertEquals(lj['state'], 'active')
        self.assertSameTimes(lj['active_end_dt'], self.days_ahead(3))
        self.assertSameTimes(lj['eval_end_dt'], self.days_ahead(6))

    def test_active_wrong_dt(self):
        self.add_users(1, 5)
        lj = self['t1'].create_lesson_with_questions('ספרות', title='שיעור ראשון')
        url = lj['resource_url']
        lj = self['t1'].post(url + 'activate/',
                             data={'active_end_dt': self.days_ahead(-3),
                                   'eval_end_dt': self.days_ahead(6)},
                             expected_status=400,
                             expected_error={'non_field_errors': 'זמן סיום חייב להיות בעתיד'})
        lj = self['t1'].post(url + 'activate/',
                             data={'active_end_dt': self.days_ahead(3),
                                   'eval_end_dt': self.days_ahead(-6)},
                             expected_status=400,
                             expected_error={'non_field_errors': 'זמן סיום חייב להיות בעתיד'})
        lj = self['t1'].post(url + 'activate/',
                             data={'active_end_dt': self.days_ahead(-3),
                                   'eval_end_dt': self.days_ahead(-6)},
                             expected_status=400,
                             expected_error={'non_field_errors': 'זמן סיום חייב להיות בעתיד'})

        lj = self['t1'].post(url + 'activate/',
                             data={'active_end_dt': self.days_ahead(6),
                                   'eval_end_dt': self.days_ahead(3)},
                             expected_status=400,
                             expected_error={'non_field_errors': 'זמן סיום של ההערכה חייב להיות מאוחר יותר'})

    def test_other_teacher(self):
        self.add_users(2, 5)
        lj = self['t1'].create_lesson_with_questions('ספרות', title='שיעור ראשון')
        url = lj['resource_url']
        lj = self['t2'].post(url + 'activate/',
                             data={'active_end_dt': self.days_ahead(3),
                                   'eval_end_dt': self.days_ahead(6)},
                             expected_status=403)

    def test_student(self):
        self.add_users(2, 5)
        lj = self['t1'].create_lesson_with_questions('ספרות', title='שיעור ראשון')
        url = lj['resource_url']
        lj = self['s1'].post(url + 'activate/',
                             data={'active_end_dt': self.days_ahead(3),
                                   'eval_end_dt': self.days_ahead(6)},
                             expected_status=403)
