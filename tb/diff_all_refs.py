#!/usr/bin/env python
import glob
import os

TB_DIR = os.path.dirname(__file__)

def get_content(f):
    with open(f) as fh:
        content = fh.read()
    return content

def yes_no_input(title):
    while True:
        res = input(title).lower()
        if res in ['y','yes']:
            return True
        if res in ['n','no','not']:
            return False

def main(differ):
    for f in glob.glob(os.path.join(TB_DIR,'ref/*.log')):
        ref_file = f
        filtered_file = os.path.join(os.path.join(TB_DIR,'filtered/',os.path.basename(f)))
        f1 = get_content(ref_file)
        f2 = get_content(filtered_file)
        print('='*60)
        print('compare {0} {1}'.format(filtered_file,ref_file))
        if f1 != f2:
            os.system('{differ} {0} {1}'.format(filtered_file,ref_file,differ=differ))
            res = yes_no_input('Do you want to update? y/n ')
            if res:
                os.system('cp {0} {1}'.format(filtered_file,ref_file))
                print('updated...')
                

import argparse
parser = argparse.ArgumentParser()
parser.add_argument('--differ',default='diff')
ns = parser.parse_args()
main(ns.differ)


        
