from django.conf import settings
from rest_framework import serializers

from . import models


class ModifiedFileField(serializers.FileField):
    def to_representation(self, value):
        if not value:
            return None
        url = value.url
        return '{0}{1}'.format(settings.HOST_URL, url)


class MediaSerializer(serializers.ModelSerializer):
    media = ModifiedFileField()

    class Meta:
        model = models.Media
        fields = ('media',)

