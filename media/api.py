from rest_framework import viewsets, mixins

from lomda.permissions import TeacherOnlyPermission
from . import serializers


class MediaViewSet(mixins.CreateModelMixin, viewsets.GenericViewSet):
    permission_classes = (TeacherOnlyPermission, )
    serializer_class = serializers.MediaSerializer


