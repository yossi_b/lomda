# README #

# setup

* python3
* create virtualenv

then

```
#!bash

cd lomda
python -m pip install -r requirements.txt 

```

#initialize dev environment



```
#!bash
git pull
python manage.py migrate
rm sqlite3.db
python manage.py init_env
```
#updating dev environment
```
git pull
python manage.py migrate
```

**Note:** Since we are not in production yet, if you get into errors, just init the dev environment again



# unit testing
```
python manage.py test
```

# local settings

You need to create local_settings.py depending on the environment that you are using.
You need to set at least the following:
```
HOST_NAME = 'lomda.com' 

DEPLOY_MODE = False
DEV_MODE = False
QA_MODE = True
```

* QA mode - real server, but not production
* DEV mode - local computer
* DEPLOY mode - production

* HOST_NAME name of the host

There are some templates, under lomda/lomda