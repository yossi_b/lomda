/*jshint node: true */
/*jslint node: true */


module.exports = function (grunt) {
    "use strict";

    // Project configuration.
    grunt.initConfig({

        // Metadata.

        // Task configuration.
        cssflip: {
            lomda: {
                files: [{
                    expand: true,
                    cwd: 'css',
                    dest: 'css',
                    ext: '.rtl.css',
                    src: 'lomda.css'
                },
                    {
                        expand: true,
                        cwd: './bower_components/bootstrap/dist/css/',
                        dest: './bower_components/bootstrap/dist/css/',
                        ext: '.rtl.css',
                        src: 'bootstrap.css'
                    },
                {
                        expand: true,
                        cwd: './bower_components/bootstrap/dist/css/',
                        dest: './bower_components/bootstrap/dist/css/',
                        ext: '.rtl.css',
                        src: 'bootstrap-theme.css'
                }]
            },
        },
        watch: {
            'cssflip' : {
                files: 'css/lomda.css',
                tasks: 'default',
            },
        }

    });


    // These plugins provide necessary tasks.
    grunt.loadNpmTasks('grunt-css-flip');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.registerTask('lomda', ['cssflip:lomda']);
    grunt.registerTask('default', ['lomda']);

};

