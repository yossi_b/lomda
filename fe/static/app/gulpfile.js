var gulp = require('gulp');
var flipper = require('gulp-css-flipper');
var rename = require('gulp-simple-rename');
var gutil = require('gulp-util');


// Process Styles


var renameRtl = function() {
    return rename(function (path) {
        gutil.log('path = ' + path);
        return path.replace(/\.css$/,'.rtl.css');
    });
}

gulp.task('flipLomda', function() {
    return gulp.src('./css/lomda.css')
        .pipe(flipper())
        .pipe(renameRtl())
        .pipe(gulp.dest('./css/'))
});

gulp.task('flipBS1', function() {
    return gulp.src('./bower_components/bootstrap/dist/css/bootstrap.css')
        .pipe(flipper())
        .pipe(renameRtl())
        .pipe(gulp.dest('./bower_components/bootstrap/dist/css/'))
});

gulp.task('flipBS2', function() {
    return gulp.src('./bower_components/bootstrap/dist/css/bootstrap-theme.css')
        .pipe(flipper())
        .pipe(renameRtl())
        .pipe(gulp.dest('./bower_components/bootstrap/dist/css/'))
});

gulp.task('flip',['flipBS1','flipBS2','flipLomda'])

gulp.task('watch', function() {
       gulp.watch('css/lomda.css', ['flipLomda']);
});

gulp.task('default',['flip'])




