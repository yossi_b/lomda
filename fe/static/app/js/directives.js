"use strict";
app = angular.module('lomda');

app.directive('fileUpload', function () {
    return {
        scope: {
            fileSelected: '=cb'
        }, // create a new scope
        link: function (scope, el, attrs) {
            var name = attrs.name;
            el.bind('change', function (event) {
                var files = event.target.files;
                scope.$apply(function () {
                    scope.fileSelected(name, files[0], el);
                });
            });
        }
    };
}).directive('lomdaErrors',function() {
    return {
        scope: false,
        template: function(element,attrs) {
            var field = attrs.field || 'errors';
            return '<div ng-show="' + field + '"><uib-alert type="danger" close="' + field + '=null">' +
            '<div ng-repeat="error in ' + field + '">' +
            '{{ error }}' +
            '</div>' +
            '</uib-alert>' +
            '</div>';
        }
    }
}).directive('lessonState',function() {
    return {
        scope: false,
        template: function(element,attrs) {
            var result = '<div class="label label-{{' + attrs.lessonState + '| stateColor }}">' +
                     '{{' + attrs.lessonState +  '| stateName }}' +
                     '</div>';
            return result;
        }
    }
}).directive('timeLeft',function() {
    return {
        scope: {
            dt:'=dt',
            step:'@step',
        },

        controller: function ($scope, $element, $attrs,$interval,utils) {
            $scope.refresh = function () {
                if ($scope.dt) {
                    $scope.timeLeft = utils.timeDelta($scope.dt);
                }
            };
            $scope.step = $scope.step || 1;
            $scope.step = parseInt($scope.step);
            $scope.$watch('dt',function() {
                $scope.refresh();
            });
            var interval = $interval($scope.refresh,1000*$scope.step);
            $scope.$on("$destroy", function() {
                if (interval) {
                    $interval.cancel(interval);
                    console.log('interval destroyed');
                }
            });
        },
        template: '<span ng-if="timeLeft.sign>0">' +
                  'בעוד ' +
                  '<span ng-show="timeLeft.days>0">{{timeLeft.days}} ימים </span>' +
                  '<span ng-show="timeLeft.days>0 || timeLeft.hours > 0">{{timeLeft.hours}} שעות </span>' +
                  '<span ng-show="timeLeft.days>0 || timeLeft.hours > 0 || timeLeft.minutes > 0">{{timeLeft.minutes}} דקות </span>' +
                  '<span ng-show="step < 60"> {{timeLeft.seconds}} שניות </span>' +
                  '</span>' +
                   '<span ng-if="timeLeft.sign < 0">' +
                    'הזמן עבר...' +
                   '</span>'
    }
}).directive('dtSelector',function() {
    var range = function(x,y) {
        var result = [];
        while (x <= y) {
            result.push(x);
            x++;
        }
        return result;
    };
    return {
        scope: {
            dt: "=dtModel",
        },
        controller: function ($scope, $element, $attrs) {
            $scope.options = {
                y: range(2015, 2020),
                m: range(1, 12),
                d: range(1, 31),
                H: range(0, 23),
                M: range(0, 59)
            };
            $scope.refreshInput = function() {
                $scope.input = {
                    y: $scope.dt.getFullYear(),
                    m: $scope.dt.getMonth() + 1,
                    d: $scope.dt.getDate(),
                    H: $scope.dt.getHours(),
                    M: $scope.dt.getMinutes(),
                };
            }
            if ($scope.dt) {
                if (angular.isString($scope.dt)) {
                    try {
                        $scope.dt = new Date($scope.dt);
                    } catch(e) {
                        console.log('Error in controller', e);
                        $scope.dt = new Date();
                    }
                }
            } else {
                $scope.dt = new Date();
            }
            $scope.refreshInput();
            $scope.$watch('dt',function() {
                //console.log($scope.dt);
                $scope.refreshInput();
            });
            $scope.$watch('input',function() {
                $scope.dt = new Date($scope.input.y, $scope.input.m-1,$scope.input.d,$scope.input.H,$scope.input.M);
            },true);
            $scope.f2 = function(n) {
                if (n < 10) {
                    return '' + '0' + n;
                }
                return n;
            };
        },
        template:
            '<span ng-class="\'w10\'"> יום  {{ dt.getDay() | dayName0 }}, </span>' +
            '<select ng-class="\'w4\'" ng-options="y for y in options.y" ng-model="input.y"></select>/' +
            '<select ng-class="\'w2\'" ng-options="m for m in options.m" ng-model="input.m"></select>/' +
            '<select ng-class="\'w2\'" ng-options="d for d in options.d" ng-model="input.d"></select>' +
             'שעה:' +
            '<select ng-class="\'w2\'" ng-options="f2(M) for M in options.M" ng-model="input.M"></select>:'+
            '<select ng-class="\'w2\'" ng-options="f2(H) for H in options.H" ng-model="input.H"></select>' +
            '',
    }
});

app.directive('ytLink',function(utils) {
    return {
        scope: {
            ytVid:'@vid',
        },
        controller: function ($scope, $element, $attrs) {
            $scope.refresh = function() {
                $scope.ytLink = utils.youtubeIdToLink($scope.ytVid)
            }
            $scope.$watch('ytVid',$scope.refresh);
        },
        template: '<div class="youtube" ng-if="ytVid">' +
                    '<div class="embed-responsive embed-responsive-16by9">' +
                     '<iframe class="embed-responsive-item" id="ytplayer" type="text/html" ng-src="{{ytLink}}"></iframe>' +
                    '</div></div>',
    }
});

app.directive('notSubmitted',function() {
    return {
        restrict: 'A',
        replace: false,
        template: '<span class="label label-danger">לא הוגש</span>'
    }
});

app.directive('score',function() {
    return {
        restrict: 'A',
        replace: false,
        scope: {
            value:'@value',
        },
        template: '<span><span ng-show="!value">אין ציונים</span>' +
            '<span ng-hide="!value">{{value | scoreNumber}}</span></span>',
    }
});

app.directive('bool2vx',function() {
    return {
        restrict: 'A',
        replace: true,
        scope: {
            value:'=value',
            valid:'=valid'
        },
        controller: function ($scope, $element, $attrs) {
            $scope.icon = $scope.value ? 'ok' : 'remove';
            $scope.text = $scope.value ? 'success': 'danger';
        },
        template: '<span>' +
        '<span ng-show="valid" class="glyphicon glyphicon-{{icon}} text-{{text}}"><span>' +
        '</span>'
    }
});

app.directive('ready',function() {
    return {
        restrict: 'A',
        replace: false,
        transclude: true,
        scope: {
            ready:'=ready',
        },
        controller: function($scope, $element, $attrs) {
            $scope.dummy = true;
        },
        template: '<div ng-cloak><div ng-if="ready" ng-cloak><ng-transclude></ng-transclude></div><div ng-if="!ready"><span us-spinner></span></div>'
    }
})


