"use strict";

app = angular.module('lomda');

app.service('utils', function ($sce) {
    return {
        isLegalScore: function(ni) {
            if (!ni) {
                return false;
            }
            var n = parseInt(ni);
            if (!n || isNaN(n)) {
                return false;
            }
            if (n < 1 || n > 10) {
                return false;
            }
            return true;
        },
        combineDateTime: function (dt1, dt2) {
            var y = dt1.getFullYear();
            var m = dt1.getMonth();
            var d = dt1.getDate();
            var H = dt2.getHours();
            var M = dt2.getMinutes();
            return new Date(y, m, d, H, M);
        },
        find: function(arr,pred) {
            for (var i = 0 ; i < arr.length; i++) {
                if (pred(arr[i])) {
                    return arr[i];
                }
            }
            return undefined;
        },
        findIndex: function(arr,pred) {
            for (var i = 0 ; i < arr.length; i++) {
                if (pred(arr[i])) {
                    return i;
                }
            }
            return undefined;
        },
        extractErrors: function (resp) {
            var errors = [];
            if (angular.isObject(resp.data)) {
                angular.forEach(resp.data, function (v, k) {
                    if (angular.isArray(v)) {
                        v.forEach(function(e) {
                            if (k!='non_field_errors') {
                                e = k + ': ' + e;
                            }
                            errors.push(e);
                        });
                    } else {
                        if (k!='non_field_errors') {
                            v = k + ': ' + v;
                        }
                        errors.push(v);
                    }
                });
            } else {
                if (resp.status < 0) {
                    errors.push('server unreachable');
                }
                if (resp.status == 404) {
                    errors.push('resource not found');
                }
                else if (resp.status == 500) {
                    errors.push('internal error');
                }
                else {
                    errors.push(resp.data);
                }
            }
            if (errors.length > 0) {
                var uniqueErrors = [];
                errors.forEach((function (e) {
                    if (uniqueErrors.indexOf(e) < 0) {
                        uniqueErrors.push(e);
                    }
                }));
                return uniqueErrors;
            }
            return null;
        },
        youtubeIdToLink: function (vid, raw) {
            // get youtube id e.g B0VQd1uXb5o
            // and return the embed link trusted
            // return null if vid is null
            if (vid == null) {
                return null;
            }
            return $sce.trustAsResourceUrl('https://www.youtube.com/embed/' + vid);
        },
        extractYoutubeId: function (link) {
            // get youtube link (e.g. https://www.youtube.com/watch?v=B0VQd1uXb5o)
            // and returns the id (e.g. B0VQd1uXb5o)
            var re = /https?:.*youtube.com.*[\?\&]v=([^\?\&]*)/;
            var match = re.exec(link);
            if (match) {
                return match[1];
            }
            return null;
        },
        timeDelta: function (dt) {
            function div(x, y) {
                var mod = x % y
                var div = (x - x % y) / y;
                return div;
            }

            function mod(x, y) {
                return x % y;
            }

            var now = new Date().getTime();
            if (!dt) {
                return null;
            }
            var future = new Date(dt).getTime();
            var delta = Math.floor((future - now) / 1000);
            var sign = 1;
            if (delta < 0) {
                delta = -delta;
                sign = -1;
            }
            var result = {
                sign: sign
            };
            result.sign = sign;
            result.seconds = mod(delta, 60);
            var left = div(delta, 60);
            result.minutes = mod(left, 60);
            left = div(left, 60)
            result.hours = mod(left, 24)
            result.days = div(left, 24)
            return result;
        }
    }
});


app.service('db', function ($window) {
    var service = {
        clear: function() {
            this.data = {};
            $window.localStorage.clear();
        },
        data: {}
    };
    service.common = {
        setUser: function (user) {
            service.data.user = user;
            $window.localStorage.setItem('lomda:user', angular.toJson(user))
        },
        getUser: function () {
            return service.data.user;
        },
        setCourse: function (course) {
            service.data.course = course;
        },
        getCourse: function () {
            return service.data.course;
        },
    };
    service.teacher = {
    }
    service.student = {
    }
    var lsUser = $window.localStorage.getItem('lomda:user');
    if (lsUser) {
        service.data.user = angular.fromJson(lsUser);
    }
    return service;
});

app.service('myhttp',function($http,db,$state,$q) {
    return {
        http: function (config) {
            var headers = undefined;
            if (!config.noAuth) {
                if (!db.common.getUser()) {
                    return $state.go('login');
                }
                headers = {
                    Authorization: 'Token ' + db.common.getUser().token
                }
            }
            var httpConf = {
                method: config.method.toUpperCase(),
                url: config.url,
                data: config.data,
                params:config.params,
                headers: headers,
            };
            if (config.hasFile) {
                var fd = new FormData();
                angular.forEach(config.data, function (v, k) {
                    fd.append(k,v);
                });
                httpConf['data'] = fd;
                httpConf['transformRequest'] = angular.identity;
                headers = headers || {};
                headers['Content-Type'] = undefined;
                httpConf['headers'] = headers;
            }
            return $http(httpConf).then(function(resp) {
                return $q.resolve(resp);
            },function(resp) {
                if ((resp.status == 401 || resp.status == 403) && !config.noAuth) {
                    $state.go('login');
                }
                return $q.reject(resp);
            });
        },
        get: function (url, params, config) {
            config = config || {};
            config.method = 'get';
            config.url = url;
            config.params = params;
            return this.http(config);
        },
        post: function (url, data, config) {
            config = config || {};
            config.method = 'post';
            config.url = url;
            config.data = data;
            return this.http(config);
        },
        'delete': function(url,data,config) {
            config = config || {};
            config.method = 'delete';
            config.url = url;
            config.data = data;
            return this.http(config);
        }
    }
});

app.service('api', function($http,db,$q,myhttp) {
    var common = {
        getUser: function () {
            return myhttp.get('/api/users/self/').then(
                function (result) {
                    return result.data;
                }
            )
        },
        login: function (email, password) {
            return myhttp.post('/api/users/login/', {
                email: email,
                password: password
            }, {
                noAuth: true
            }).then(function (result) {
                return result.data;
            })
        },
        uploadFile: function (file) {
            return myhttp.post('/api/media/', {
                media: file
            }, {
                hasFile: true
            }).then(function (result) {
                return result.data;
            });
        },
        getCurrentCourse: function(config) {
            config = config || {};
            if (!config.forceServer) {
                var course = db.common.getCourse();
                if (course) {
                    return $q.resolve(course);
                }
            }
            return myhttp.get('/api/courses/current/',null).then(function(result) {
                db.common.setCourse(result.data);
                return result.data
            })
        },
        getLesson: function(courseUrl, lessonId) {
            return myhttp.get(courseUrl + 'lessons/' + lessonId + '/')
                .then(function(result) {
                    return result.data;
                });
        },
    };
    var teacher = {
        getLessons: function(courseUrl) {
              return myhttp.get(courseUrl + 'lessons/')
                .then(function(result) {
                    return result.data;
                });
        },
        getLessonQuestions: function(lessonUrl) {
            return myhttp.get(lessonUrl + 'questions/')
                .then(function(result) {
                    return result.data;
                });
        },
        getLessonQuestionsFull: function(lessonUrl,params) {
            return myhttp.get(lessonUrl + 'questions/full/',params)
                .then(function(result) {
                    return result.data;
                });
        },
        getLessonRecords: function(lessonUrl) {
            return myhttp.get(lessonUrl + 'student-records/teacher/')
                .then(function(result) {
                    return result.data;
                });
        },
        getLessonRecord: function(lessonUrl,lrId) {
            return myhttp.get(lessonUrl + 'student-records/' + lrId + '/user/')
                .then(function(result) {
                    return result.data;
                });
        },
        getCourseQuestionsFull: function(courseUrl,crId,kind) {
            return myhttp.get(courseUrl + 'questions/full/',{
                kind:kind,
                cr_id:crId
            }).then(function(result) {
                return result.data;
            });
        },
        getCourseStudentsRecords: function(courseUrl) {
            return myhttp.get(courseUrl + 'student-records/')
                .then(function(result) {
                    return result.data;
                });
        },
        getCourseStudentsRecord: function(courseUrl,crId) {
            return myhttp.get(courseUrl + 'student-records/' + crId + '/')
                .then(function(result) {
                    return result.data;
                });
        },
        createQuestion: function(lessonUrl, text) {
            return myhttp.post(lessonUrl + 'questions/',{
                'text':text
            });
        },
        editQuestion: function (questionUrl, text) {
            return myhttp.post(questionUrl + 'edit/', {
                'text': text
            });
        },
        deleteQuestion: function(questionUrl) {
            return myhttp.delete(questionUrl);
        },
        setQuestionOrAnswerTeacherScore: function(questionUrl, answerId, score) {
            // answerId maybe undefined if this is question
            return myhttp.post(questionUrl + 'set-teacher-score/',{
                answer_id: answerId,
                score: score
            }).then(function(result) {
                return result.data;
            })
        },
        deleteQuestionOrAnswerTeacherScore: function(questionUrl, answerId, score) {
            // answerId maybe undefined if this is question
            return myhttp.post(questionUrl + 'delete-teacher-score/',{
                answer_id: answerId,
            }).then(function(result) {
                return result.data;
            })
        },
        createLesson: function(courseUrl, data) {
            return myhttp.post(courseUrl + 'lessons/', data).then(function (result) {
                return result.data;
            })
        },
        activate: function(lessonUrl, activeEndDt, evalEndDt) {
            return myhttp.post(lessonUrl + 'activate/', {active_end_dt:activeEndDt, eval_end_dt: evalEndDt}).then(function (result) {
                return result.data;
            });
        },
        eval: function(lessonUrl, endDt) {
            return myhttp.post(lessonUrl + 'evaluate/', {eval_end_dt:endDt}).then(function (result) {
                return result.data;
            });
        },
        close: function(lessonUrl) {
            return myhttp.post(lessonUrl + 'close/').then(function (result) {
                return result.data;
            });
        },
        updateLesson: function(lessonUrl, data) {
            return myhttp.post(lessonUrl + 'edit/', data,{hasFile:data.image}).then(function (result) {
                return result.data;
            });
        },
        deleteLessonField: function(lessonUrl, field) {
            var data = {};
            data[field] = '----';
            return myhttp.post(lessonUrl + 'delete-field/',data).then(function (result) {
                return result.data;
            })
        }
    };
    var student = {
        getLessonRecord: function(courseUrl, lessonId) {
            return myhttp.get(courseUrl + 'lessons/' + lessonId + '/student-records/mine/').then(function(result) {
                return result.data
            })
        },
        getCourseRecord: function(courseUrl) {
            return myhttp.get(courseUrl + 'student-records/mine/').then(function(result) {
                return result.data
            })
        },
        getCourseRecordSummary: function(courseUrl) {
            return myhttp.get(courseUrl + 'student-records/mine/summary/').then(function(result) {
                return result.data
            })
        },
        askQuestion: function(recordUrl, text) {
            return myhttp.post(recordUrl + 'ask-question/',{
                'text':text
            }).then(function(result) {
                return result.data;
            });
        },
        answerQuestions: function(recordUrl, answers) {
            return myhttp.post(recordUrl + 'answer-questions/',{
              answers: answers
            }).then(function(result) {
                return result.data;
            });
        },
        sendEvals: function(recordUrl,qEval,aEval) {
            return myhttp.post(recordUrl+'eval/',{
                questions: qEval,
                answers: aEval
            }).then(function(result) {
                return result.data;
            });
        },
        getLessonQuestionsStudent: function(lessonUrl) {
            return myhttp.get(lessonUrl + 'questions/student-view/')
                .then(function(result) {
                    return result.data;
                })
        }
    };
    return {
        teacher: teacher,
        student: student,
        common: common
    }
});
