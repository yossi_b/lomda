"use strict";
var app = angular.module('lomda');

app.controller("ContainerController", ['$scope', '$state', 'api', 'db',
    function ($scope, $state, api, db) {
        $scope.go = function (goto,params) {
            //console.log('goto ' + goto);
            $state.go(goto, params);
        }
        $scope.forceParam = function(p) {
            if (angular.isUndefined(p) || angular.isString(p) && p == '') {
                console.log('param is undefined. Go to welcome page')
                $scope.go('welcome');
                throw 'go other place';
            }
        }
    }]);

app.controller('NavController',['$scope','$rootScope','$interval','api','db',
    function($scope,$rootScope,$interval,api, db) {
        $scope.$on('$stateChangeStart', function (event, toState, toParams) {
            $scope.active = toState.active || 'home';
            //console.log('active = ' + $scope.active);
        });
        $scope.$on('EV_LOGIN_LOGOUT',function(ev,params) {
            $scope.removeInterval();
            $scope.refresh();
        });
        $scope.interval = null;
        $scope.refresh = function() {
            $scope.user = db.common.getUser();
            if ($scope.user) {
                api.common.getCurrentCourse().then(function (course) {
                    $scope.course = course;
                    if ($scope.user.is_student) {
                        api.student.getCourseRecordSummary($scope.course.resource_url).then(function (record) {
                            $scope.record = record;
                        })
                    }
                });
                if (!$scope.interval) {
                    $scope.interval = $interval($scope.refresh, 60*1000);
                }
            }


        };
        $scope.removeInterval = function() {
            if ($scope.interval) {
                $interval.cancel($scope.interval);
                console.log('navbar interval destroyed');
            }
        }
        $scope.$on("$destroy", function() {
            $scope.removeInterval();
        });
        $scope.refresh();
    }]);

app.controller("WelcomeController",["$scope","$state","db",
function($scope,$state,db) {
    if (db.common.getUser()) {
        var user = db.common.getUser();
        if (user.is_student) {
            $state.go('student_welcome');
        } else if (user.is_teacher) {
            $state.go('teacher_welcome');
        } else {
            alert('illegal user - not student/teacher');
        }
    } else {
        $state.go('login');
    }
}]);

app.controller('LoginController', ['$scope', '$state', '$rootScope', 'api', 'db','utils',
    function ($scope, $state, $rootScope, api, db, utils) {
        $scope.input = {
            email: 't1@lomda.com',
            password: 'Teacher123'
        }
        $scope.login = function () {
            $scope.authError = false;
            api.common.login($scope.input.email, $scope.input.password).then(function (user) {
                db.clear();
                db.common.setUser(user);
                if ($rootScope.prevState) {
                    $state.go($rootScope.prevState);
                } else {
                    $rootScope.$broadcast('EV_LOGIN_LOGOUT',{'login':true});
                    $state.go('welcome');
                }
            },function(resp) {
                if (resp.status == 401) {
                    $scope.errors = ["ההזדהות נכשלה. אנא בדוק שם וססמא שוב"];
                } else {
                    $scope.errors = utils.extractErrors(resp);
                }
            })
        }
    }]);


app.controller('LogoutController', ['$scope', '$state', '$rootScope', 'api', 'db',
    function ($scope, $state, $rootScope, api, db) {
        db.clear();
        $rootScope.$broadcast('EV_LOGIN_LOGOUT',{'login':false});
    }]);
