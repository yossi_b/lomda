"use strict";

var app = angular.module('lomda', ['ui.router', 'ui.bootstrap','angularSpinner']);


app.run(function($state, $rootScope, $location, db) {
    $rootScope.$on('$stateChangeStart', function (event, toState, toParams) {
        //console.log('In rootScope: $on($stateChangeStart');
        if (!toState.noAuth && !db.common.getUser()) {
            $rootScope.prevState = toState.name;
            $location.path('/login');
        }
    });
    return;
})

app.config(function ($stateProvider, $urlRouterProvider) {

    // For any unmatched url, send to /route1
    $urlRouterProvider.otherwise("/welcome")

    var fullUrl = function(f) {
        return '/static/app/' + f;
    }


    $stateProvider
        .state('welcome', {
            url: "/welcome",
            controller: 'WelcomeController',
            noAuth: true,
        })
        .state('login', {
            url: "/login",
            templateUrl: fullUrl("partials/common/login.html"),
            controller: "LoginController",
            noAuth: true,
        })
        .state('about', {
            url: "/about",
            templateUrl: fullUrl('partials/common/about.html'),
            noAuth: true,
            active: 'about',
        })
        .state('contact', {
            url: "/contact",
            templateUrl: fullUrl('partials/common/contact.html'),
            noAuth: true,
            active: 'contact',
        })
        .state('logout', {
            url: "/logout",
            templateUrl: fullUrl('partials/common/logout.html'),
            controller: 'LogoutController',
            noAuth: true,
            active: 'logout',
        })
        .state('teacher_welcome', {
            url: "/teacher/welcome",
            templateUrl: fullUrl("partials/teacher/welcome.html"),
            controller: "TeacherWelcomeController",
            active: "teacher",
        })
        .state('teacher_current_lesson', {
            url: "/teacher/lessons/current",
            controller: "TeacherCurrentLessonController",
            templateUrl: fullUrl("partials/common/no_current_lesson.html"),
            active: "teacher",
        })
        .state('teacher_new_lesson', {
            url: "/teacher/lessons/new",
            templateUrl: fullUrl("partials/teacher/new_lesson.html"),
            controller: 'TeacherNewLessonController',
            active: "teacher",
        })
        .state('teacher_lessons', {
            url: "/teacher/lessons",
            templateUrl: fullUrl("partials/teacher/lessons.html"),
            controller: 'TeacherLessonsController',
            active: "teacher",
        })
        .state('teacher_lesson', {
            url: "/teacher/lessons/{lessonId}",
            templateUrl: fullUrl("partials/teacher/lesson.html"),
            controller: 'TeacherLessonController',
            active: "teacher",
        })
        .state('teacher_lesson_questions', {
            url: "/teacher/lessons/{lessonId}/questions",
            templateUrl: fullUrl("partials/teacher/lesson_questions.html"),
            controller: 'TeacherQuestionsPageController',
            active: "teacher",
        })
        .state('teacher_lesson_questions_filtered', {
            url: "/teacher/lessons/{lessonId}/questions/{slId}/{kind}",
            templateUrl: fullUrl("partials/teacher/lesson_questions.html"),
            controller: 'TeacherQuestionsPageController',
            active: "teacher",
        })
        .state('teacher_lesson_status', {
            url: "/teacher/lessons/{lessonId}/status",
            templateUrl: fullUrl("partials/teacher/lesson_status.html"),
            controller: 'TeacherLessonStatusPageController',
            active: "teacher",
        })
        .state('teacher_students', {
            url: "/teacher/students",
            templateUrl: fullUrl("partials/teacher/students.html"),
            controller: 'TeacherStudentsController',
            active: "teacher",
        })
        .state('teacher_course_questions_filter', {
            url: "/teacher/course/{courseId}/questions/{courseRecordId}/{kind}",
            templateUrl: fullUrl("partials/teacher/course_questions.html"),
            controller: 'TeacherCourseQuestionsFilterController',
            active: "teacher",
        })
        .state("student_welcome",{
            url: "/student/welcome",
            templateUrl: fullUrl("partials/student/welcome.html"),
            controller: "StudentWelcomeController",
            active: "student",
        })
        .state("student_current_lesson",{
            url: "/student/lessons/current",
            templateUrl: fullUrl("partials/common/no_current_lesson.html"),
            controller: "StudentCurrentLessonController",
            active: "student",
        })
        .state('student_lessons', {
            url: "/student/lessons",
            templateUrl: fullUrl("partials/student/lessons.html"),
            controller: 'StudentLessonsController',
            active: "student",
        })
        .state('student_lesson', {
            url: "/student/lessons/{lessonId}",
            templateUrl: fullUrl("partials/student/lesson.html"),
            controller: 'StudentLessonController',
            active: "student",
        })
        .state('student_lesson_questions', {
            url: "/student/lessons/{lessonId}/questions",
            templateUrl: fullUrl("partials/student/lesson_questions.html"),
            controller: 'StudentQuestionsPageController',
            active: "student",
        })
});

