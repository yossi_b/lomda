"use strict";

app.constant('STATES',{
        'initial': ['התחלתי','warning'],
        'active': ['פעיל','success'],
        'eval': ['בהערכה','success'],
        'close': ['סגור','danger'],
        'default': ['']
    });

app.constant('STUNDET_LESSON_DESCRIPTION',{
    'ask': 'שלב כתיבת שאלה',
    'answer':'שלב מענה לשאלות',
    'eval': 'שלב הערכת חברים',
    'done': 'משימות הסתיימו'
});

app.constant('DAYNAMES',
   ['ראשון',
    'שני',
    'שלישי',
   'רביעי',
   'חמישי',
   'שישי',
   'שבת']
);

app.constant('QUESTION_KIND', {
    'eqa': 'הערכות',
    'qa': 'שאלות ותשובות',
});

app.filter('yesNo',function() {
    return function(val) {
            return val ? 'כן' : 'לא';
    }
}).filter('lomdaDate',function($filter) {
    return function(dt) {
        if (!dt) {
            return '';
        }
        return $filter('date')(dt,'dd/MM/yyyy HH:mm');
    }
}).filter('stateName',function(STATES) {
    return function(state) {
        return (STATES[state] || [state])[0];
    }
}).filter('stateColor',function(STATES) {
    return function(state) {
        return (STATES[state] || [null,'primary'])[1];
    }
}).filter('dayName0',function(DAYNAMES) {
    return function(d) {
        return DAYNAMES[d] || d;
    }
}).filter('studentLessonDescription',function(STUNDET_LESSON_DESCRIPTION) {
    return function(st) {
        return STUNDET_LESSON_DESCRIPTION[st] || st;
    }

});

app.filter('scoreNumber',function($filter) {
    return function(s) {
        if (s == Math.round(s)) {
            return s
        }
        if (s*10 == Math.round(s*10)) {
            return $filter('number')(s,1);
        };
        return $filter('number')(s,2);
    }
});

