"use strict";

app.controller('TeacherWelcomeController', ['$scope', '$state', 'api', 'db',
    function ($scope, $state, api, db) {
    }]);

app.controller('TeacherCurrentLessonController',['$scope', '$state', 'api', 'db',
    function ($scope, $state, api, db) {
        api.common.getCurrentCourse({forceServer:true}).then(function(course) {
            if (course.current_lesson) {
                $scope.go('teacher_lesson',{'lessonId': course.current_lesson.id});
            } else {
                $scope.noActiveLesson = true;
            }
        });
    }]);

app.controller('TeacherNewLessonController', ['$scope', '$state', 'api', 'db', 'utils',
    function ($scope, $state, api, db, utils) {
        api.common.getCurrentCourse().then(function(course) {
            $scope.course = course;
        });
        $scope.input = {
            link: null,
        };
        $scope.dtStartInput = {};
        $scope.dtEndInput = {};
        $scope.input.youtubeId = null;
        $scope.ytVid = null;
        $scope.ytChange = function () {
            $scope.ytVid = utils.extractYoutubeId($scope.input.link);
            $scope.input.youtube_id = $scope.ytVid;
        };
        $scope.fileSelected = function (name, file, el) {
            api.common.uploadFile(file).then(function(media) {
                $scope.input.image = media.media;
                el.val('');
            })
        };
        $scope.clearSelectedFile = function () {
            $scope.input.image = null;
        };
        $scope.createLesson = function () {
            var data = {
                title: $scope.input.title,
                content: $scope.input.content,
            };
            if ($scope.input.youtube_id) {
                data.youtube_id = $scope.input.youtube_id;
            }
            if ($scope.input.image) {
                data.image = $scope.input.image;
            }

            api.teacher.createLesson($scope.course.resource_url, data).then(function(data)  {
                $scope.go('teacher_lesson',{'lessonId': data.id});
            },function(resp) {
                $scope.errors = utils.extractErrors(resp);
            });
        }
    }]);


app.controller('TeacherLessonsController',['$scope', '$state', '$stateParams', 'api','db','utils',
    function ($scope, $state, $stateParams, api, db,utils) {
        api.common.getCurrentCourse().then(function (course) {
            $scope.course = course;
            api.teacher.getLessons($scope.course.resource_url).then(function(lessons) {
               $scope.lessons = lessons;
            });
        });

    }]);

app.controller('TeacherLessonController', ['$scope', '$state', '$stateParams', '$timeout','api', 'db', 'utils',
    function ($scope, $state, $stateParams, $timeout, api, db, utils) {
        $scope.lessonId = $stateParams.lessonId;
        $scope.input = {};
        $scope.editMode = {};
        $scope.errors = {};
        api.common.getCurrentCourse().then(function (course) {
            $scope.course = course;
            api.common.getLesson($scope.course.resource_url, $scope.lessonId).then(function (lesson) {
                $scope.refreshLesson(lesson);
                $scope.refreshQuestions();
            });
        });
        $scope.refreshLesson = function(lesson) {
            $scope.lesson = lesson;
            $scope.ytVid = $scope.lesson.youtube_id;
            $scope.image = lesson.image;
            $scope.canEdit = lesson.state != 'close';
            $scope.input.active_end_dt = lesson.active_end_dt;
            $scope.input.eval_end_dt = lesson.eval_end_dt;
        };
        $scope.timeChanged = function(row) {
            var t1 = new Date(row.end_dt);
            var t2 = $scope.input[row.state + '_end_dt'];
            return t1.getTime() - t2.getTime() != 0;
        }
        $scope.clearTime = function(row) {
            var t1 = new Date(row.end_dt);
            $scope.input[row.state + '_end_dt'] = t1;
        }
        $scope.editTime = function(row) {
            $scope.errors.times = null;
            var f = row.state + '_end_dt';
            var data = {};
            data[f] = $scope.input[f];
            api.teacher.updateLesson($scope.lesson.resource_url,data).then(function(lesson) {
                $scope.refreshLesson(lesson)
            },function(resp) {
                $scope.errors.times = utils.extractErrors(resp);
            });
        };
        $scope.refreshQuestions = function() {
            api.teacher.getLessonQuestions($scope.lesson.resource_url).then(function (questions) {
                $scope.questions = questions;
                $timeout(function() {
                    $scope.ready = true;
                },0);
            });
        };
        $scope.edit = function(field) {
            if (field == 'youtube_id' && $scope.input.link) {
                $scope.ytChange();
            }
            $scope.errors[field] = null;
            $scope.editMode[field] = true;
            $scope.input[field] = $scope.lesson[field]
        };
        $scope.cancel = function(field) {
            $scope.errors[field] = null;
            $scope.editMode[field] = false;
            if (field == 'youtube_id') {
                $scope.ytVid = $scope.lesson.youtube_id;
            }
            if (field == 'image') {
                $scope.image = $scope.lesson.image;
            }
        }
        $scope.approve = function(field) {
            if (field == 'youtube_id') {
                $scope.ytChange();
            }
            var data = {};
            data[field] = $scope.input[field];
            api.teacher.updateLesson($scope.lesson.resource_url, data)
                .then(function (data) {
                    $scope.errors[field] = null;
                    $scope.editMode[field] = false;
                    $scope.refreshLesson(data);
                }, function (resp) {
                    $scope.errors[field] = utils.extractErrors(resp);
                });
        };
        $scope.trash = function(field) {
            api.teacher.deleteLessonField($scope.lesson.resource_url, field)
                .then(function (data) {
                    $scope.errors[field] = null;
                    $scope.editMode[field] = false;
                    $scope.refreshLesson(data);
                }, function (resp) {
                    $scope.errors[field] = utils.extractErrors(resp);
                });
        };
        $scope.ytChange = function () {
            $scope.ytVid = utils.extractYoutubeId($scope.input.link);
            $scope.input.youtube_id = $scope.ytVid;
        };
        $scope.fileSelected = function (name, file, el) {
            api.common.uploadFile(file).then(function(media) {
                $scope.input.image = media.media;
                $scope.image = media.media;
                el.val('');
            },function(resp) {
                $scope.errors.image = utils.extractErrors(resp);
            })
        };
        $scope.moveToNextState = function() {
            $scope.errors.state = null;
            var p = null;
            if ($scope.lesson.next_state == 'active') {
                p = api.teacher.activate($scope.lesson.resource_url,$scope.input.active_end_dt,$scope.input.eval_end_dt)
            } else if ($scope.lesson.next_state == 'eval') {
                p = api.teacher.eval($scope.lesson.resource_url,$scope.input.eval_end_dt);
            } else if ($scope.lesson.next_state == 'close') {
                p = api.teacher.close($scope.lesson.resource_url);
            } else {
                $scope.errors.state = ['illegal next state: ' + lesson.next_state];
                return;
            }
            p.then(function(lesson) {
                $scope.refreshLesson(lesson);
            },function(resp) {
                $scope.errors.state = utils.extractErrors(resp);
            });
        }
    }]);

app.controller('TeacherQuestionController',['$scope', '$state', '$stateParams', 'api', 'db', 'utils',
    function ($scope, $state, $stateParams, api, db, utils) {
        $scope.input = {
        };
        $scope.editMode = false;
        $scope.errors = null;
        $scope.edit = function() {
            $scope.editMode = true;
            $scope.input.text = $scope.question.text;
        };
        $scope.trash = function() {
            $scope.errors = null;
            api.teacher.deleteQuestion($scope.question.resource_url).then(function() {
                $scope.refreshQuestions();
            },function(resp) {
                $scope.errors = utils.extractErrors(resp);
            });
        };

        $scope.approve = function() {
            $scope.errors = null;
            api.teacher.editQuestion($scope.question.resource_url, $scope.input.text).then(function () {
                $scope.refreshQuestions();
            }, function (resp) {
                $scope.errors = utils.extractErrors(resp);
            });
        }
        $scope.cancel = function() {שפ
            $scope.editMode = false;
        }
    }]);

app.controller('NewQuestionController',['$scope', '$state', '$stateParams', 'api', 'db', 'utils',
    function ($scope, $state, $stateParams, api, db, utils) {
        $scope.input = {};
        $scope.errors = null;
        $scope.editMode = false;
        $scope.addQuestion = function() {
            $scope.errors = null;
            api.teacher.createQuestion($scope.lesson.resource_url,$scope.input.text).then(function() {
                $scope.input = {};
                $scope.refreshQuestions();
            },function(resp) {
                $scope.errors = utils.extractErrors(resp);
                //console.log($scope.errors);
            });
        }
    }]);


app.controller('TeacherLessonStatusPageController',['$scope', '$state', '$stateParams', 'api', 'db', 'utils',
    function ($scope, $state, $stateParams, api, db, utils) {
        $scope.lessonId = $stateParams.lessonId;
        $scope.input = {};
        $scope.editMode = {};
        $scope.errors = {};
        $scope.states = ['answer','ask','eval'];
        api.common.getCurrentCourse().then(function(course) {
            $scope.course = course;
            api.common.getLesson($scope.course.resource_url, $scope.lessonId).then(function (lesson) {
                $scope.lesson = lesson;
                if ($scope.lesson.state == 'initial') {
                    $scope.states = [];
                } else if ($scope.lesson.state == 'active') {
                    $scope.states = ['answer','ask'] ;
                } else {
                    $scope.states = ['answer','ask','eval'] ;
                }
                $scope.refreshRecords();
            });
        });
        $scope.filter = 'all';

        $scope.valid = function(st) {
            return $scope.states.indexOf(st) >= 0;
        }
        $scope.refreshRecords = function() {
            api.teacher.getLessonRecords($scope.lesson.resource_url).then(function(records) {
                $scope.allRecords = records;
                $scope.doFilter($scope.filter);
                $scope.ready = true;
            });
        }
        $scope.doFilter = function(f) {
            $scope.filter = f;
            $scope.records = $scope.relevant($scope.allRecords);
            $scope.counters = {}
            $scope.states.forEach(function(st) {
                $scope.counters[st] = {
                    true:0,
                    false:0
                }
            });
            $scope.states.forEach(function(state) {
                $scope.records.forEach(function(sr) {
                    $scope.counters[state][sr[state + '_completed']]++;
                });
            });
        };
        $scope.isBad = function(r) {
            return !$scope.isGood(r);
        };
        $scope.isGood = function(r) {
            for (var i = 0 ; i < $scope.states.length ; i++) {
                var state = $scope.states[i];
                if (!r[state + '_completed']) {
                    return false;
                }
                return true;
            }
        };
        $scope.relevant = function(records) {
            if (!records) {
                return;
            }
            if ($scope.filter == 'all') {
                return records;
            }
            if ($scope.filter == 'bad') {
                return records.filter($scope.isBad);
            }
            if ($scope.filter == 'good') {
                return records.filter($scope.isGood);
            }
            throw 'Illegal filter ' + $scope.filter;
        }
    }]);

app.controller('TeacherQuestionsPageController',['$scope', '$state', '$stateParams','$window', 'api', 'db', 'utils','QUESTION_KIND',
    function ($scope, $state, $stateParams, $window,api, db, utils,QUESTION_KIND) {
        $scope.lessonId = $stateParams.lessonId;
        $scope.input = {};
        $scope.editMode = {};
        $scope.errors = {};
        $scope.expandAScores = {};
        $scope.expandQScores = {};
        $scope.canEditTeacherScores = true;
        $scope.canDeleteQuestions = true;
        $scope.studentLessonId = $stateParams.slId;
        $scope.kind = $stateParams.kind;
        if ($scope.kind) {
            $scope.kindName = QUESTION_KIND[$scope.kind];
        }
        $scope.deleteQuestion = function(q) {
            q.errors = null;
            api.teacher.deleteQuestion(q.resource_url).then(function() {
                $scope.refreshQuestions();
            },function(resp) {
                q.errors = utils.extractErrors(resp);
            });
        }
        api.common.getCurrentCourse().then(function(course) {
            $scope.course = course;
            api.common.getLesson($scope.course.resource_url, $scope.lessonId).then(function (lesson) {
                $scope.lesson = lesson;
                if ($scope.studentLessonId) {
                    api.teacher.getLessonRecord(lesson.resource_url,$scope.studentLessonId).then(function(sl) {
                        $scope.studentLesson = sl;
                        $scope.refreshQuestions();
                    });
                } else {
                    $scope.refreshQuestions();
                }
            });
        });

        $scope.refreshQuestions = function() {
            api.teacher.getLessonQuestionsFull($scope.lesson.resource_url,{
                student_lesson_id: $scope.studentLessonId,
                kind: $scope.kind,
            }).then(function (questions) {
                $scope.questions = questions;
                $scope.questions.forEach(function(q) {
                    $scope.refreshQuestion(q);
                })
                $scope.ready = true;
            });
        };
        $scope.changeTeacherScore = function(qa,isq) {
            if (isq &&!qa.answers) {
                throw 'error isq = true';
            }
            qa.errors = null;
            var score = qa.input_teacher_score;
            if (!utils.isLegalScore(score)) {
                qa.errors = ['ציון ריק או לא חוקי'];
                return;
            }
            var qa_id = isq ? undefined : qa.id; // only for answer
            var q = isq ? qa : qa.question;
            api.teacher.setQuestionOrAnswerTeacherScore(q.resource_url,qa_id, score).then(function(q) {
                $scope.replaceQuestion(q);
            },function(resp) {
                qa.errors = utils.extractErrors(resp);
            });
        };
        $scope.deleteTeacherScore = function(qa,isq) {
            if (isq &&!qa.answers) {
                throw 'error isq = true';
            }
            qa.errors = null;
            var qa_id = isq ? undefined : qa.id; // only for answer
            var q = isq ? qa : qa.question;
            api.teacher.deleteQuestionOrAnswerTeacherScore(q.resource_url,qa_id).then(function(q) {
                $scope.replaceQuestion(q);
            },function(resp) {
                qa.errors = utils.extractErrors(resp);
            });
        };
        $scope.cancelTeacherScore = function(qa,isq) {
            if (isq &&!qa.answers) {
                throw 'error isq = true';
            }
            qa.editTeacherScoreMode = false;
            qa.errors = null;
        };


        $scope.refreshQuestion = function(q) {
            q.answers.forEach(function(a) {
                a.question = q;
            });
            if ($scope.studentLesson) {
                var sid = $scope.studentLesson.student.id;
                var kind = $scope.kind;
                if (kind == 'qa' && q.created_by_student && q.created_by_student.id == sid) {
                    q.highlight = true;
                }
                q.evaluations.forEach(function (e) {
                    if (kind == 'eqa' && e.student && e.student.id == sid) {
                        e.highlight = true;
                    }
                });
                q.answers.forEach(function (a) {
                    if (kind == 'qa' && a.student.id == sid) {
                        a.highlight = true;
                    }
                    a.evaluations.forEach(function (e) {
                        if (kind == 'eqa' && e.student && e.student.id == sid) {
                            e.highlight = true;
                        }
                    });
                });
            }
        }
        $scope.replaceQuestion = function(question) {
            var idx = utils.findIndex($scope.questions,function(q) {
                return q.id == question.id;
            });
            $scope.questions[idx] = question;
            $scope.refreshQuestion(question);
        }
    }]);

app.controller('TeacherStudentsController',['$scope', '$state', '$stateParams', 'api', 'db', 'utils',
    function ($scope, $state, $stateParams, api, db, utils) {
        api.common.getCurrentCourse().then(function(course) {
            $scope.course = course;
            api.teacher.getCourseStudentsRecords($scope.course.resource_url).then(function(records) {
                $scope.studentRecords = records;
                $scope.flatRecords = [];
                $scope.studentRecords.forEach(function(sr) {
                    $scope.flatRecords.push(sr);
                    sr.lesson_records.forEach(function(lr) {
                       $scope.flatRecords.push(lr);
                    });
                })
            });
        });

}]);


app.controller('TeacherCourseQuestionsFilterController',['$scope', '$state', '$stateParams', '$q', 'api', 'db', 'utils','QUESTION_KIND',
    function ($scope, $state, $stateParams, $q, api, db, utils,QUESTION_KIND) {
        api.common.getCurrentCourse().then(function(course) {
                $scope.course = course;
                var courseRecordId = $stateParams.courseRecordId;
                var kind = $stateParams.kind;
                var courseId = $stateParams.courseId;
                $scope.forceParam(courseId);
                $scope.forceParam(courseRecordId);
                $scope.forceParam(kind);
                $scope.kindName = QUESTION_KIND[kind];
                $scope.forceParam($scope.kindName);
                api.teacher.getCourseStudentsRecord($scope.course.resource_url, courseRecordId).then(function(record) {
                    $scope.cr = record;
                    api.teacher.getCourseQuestionsFull($scope.course.resource_url, courseRecordId, kind).then(function (questions) {
                        $scope.questions = questions;
                        var sid = $scope.cr.student_id;
                        $scope.questions.forEach(function (q) {
                            if (kind == 'qa' && q.created_by_student && q.created_by_student.id == sid) {
                                q.highlight = true;
                            }
                            q.evaluations.forEach(function(e) {
                                if (kind == 'eqa' && e.student && e.student.id == sid) {
                                    e.highlight = true;
                                }
                            });
                            q.answers.forEach(function (a) {
                                if (kind == 'qa' && a.student.id == sid) {
                                    a.highlight = true;
                                }
                                a.evaluations.forEach(function(e) {
                                    if (kind == 'eqa' && e.student && e.student.id == sid) {
                                        e.highlight = true;
                                }
                                });
                            });
                        });
                        $scope.ready = true;
                    })
                });
        });
    }]);
