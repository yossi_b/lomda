"use strict";

app.controller('StudentWelcomeController', ['$scope', '$state', 'api', 'db',
    function ($scope, $state, api, db) {
    }]);

app.controller('StudentCurrentLessonController',['$scope', '$state', 'api', 'db',
    function ($scope, $state, api, db) {
        api.common.getCurrentCourse({forceServer:true}).then(function(course) {
            if (course.current_lesson) {
                $scope.go('student_lesson',{'lessonId': course.current_lesson.id});
            } else {
                $scope.noActiveLesson = true;
            }
        });
    }]);

app.controller('StudentLessonController',['$scope', '$state', '$stateParams','api', 'db','utils',
    function($scope,$state,$stateParams, api,db,utils) {
        $scope.lessonId = $stateParams.lessonId;
        api.common.getCurrentCourse().then(function (course) {
            $scope.course = course;
            api.student.getLessonRecord($scope.course.resource_url,$scope.lessonId).then(function(record) {
                $scope.refreshRecord(record);
            });
        });
        $scope.refreshRecord = function(record) {
            $scope.record = record;
            $scope.lesson = record.lesson;
            $scope.ready = true;
        }
    }]);


app.controller('LessonAskController',['$scope', '$state', '$stateParams','api', 'db','utils',
    function($scope,$state,$stateParams, api,db,utils) {
        $scope.input = {};
        $scope.errors = null;
        $scope.ask = function() {
            $scope.errors = null;
            api.student.askQuestion($scope.record.resource_url, $scope.input.question).then(function(record) {
                $scope.refreshRecord(record);
            },function(resp) {
                $scope.errors = utils.extractErrors(resp);
            });
        }
    }])

app.controller('LessonAnswerController',['$scope', '$state', '$stateParams','api', 'db','utils',
    function($scope,$state,$stateParams, api,db,utils) {
        $scope.input = {};
        $scope.errors = null;
        $scope.answer = function() {
            $scope.errors = null;
            var answers = {};
            $scope.record.answers.forEach(function(a) {
                var ans = $scope.input.answers[a.id];
                if (angular.isUndefined(ans)  || ans === null || ans === '') {
                    $scope.errors = ['תשובה ריקה או לא חוקית'];
                    return;
                }
                answers[a.id] = ans;
            });
            api.student.answerQuestions($scope.record.resource_url,answers).then(function(record) {
                $scope.refreshRecord(record);
            },function(resp) {
                $scope.errors = utils.extractErrors(resp);
            });
        }
    }]);

app.controller('LessonEvalController',['$scope', '$state', '$stateParams','api', 'db','utils',
    function($scope,$state,$stateParams, api,db,utils) {
        $scope.errors = null;
        $scope.input = {
            qEvals: {},
            aEvals: {}
        };
        $scope.record.evaluated_questions.forEach(function(eq) {
            $scope.input.qEvals[eq.id] = 0;
            eq.evaluatedAnswers = [];
        });
        $scope.record.evaluated_answers.forEach(function(ea) {
            var eq = utils.find($scope.record.evaluated_questions,function(q) {
                return q.question_id == ea.question_id;
            });
            eq.evaluatedAnswers.push(ea);
            $scope.input.aEvals[ea.id] = 0;
        });
        $scope.allPositive =function() {
            var result = true;
            angular.forEach($scope.input.qEvals, function(v) {
                if (v <= 0) {
                    result = false;
                }
            });
            angular.forEach($scope.input.aEvals, function(v) {
                if (v <= 0) {
                    result = false;
                }
            });
            return result;
        };
        $scope.eval = function() {
            $scope.errors = null;
            if (!$scope.allPositive()) {
                $scope.errors = ['אנא מלא את הכל'];
                return;
            }
            api.student.sendEvals($scope.record.resource_url,$scope.input.qEvals,$scope.input.aEvals).then(function(record) {
                $scope.refreshRecord(record);
            },function(resp) {
                $scope.errors = utils.extractErrors(resp);
            });;
        }
    }])

app.controller('StudentLessonsController',['$scope', '$state', '$stateParams', 'api','db','utils',
    function ($scope, $state, $stateParams, api, db,utils) {
        api.common.getCurrentCourse().then(function (course) {
            $scope.course = course;
            api.student.getCourseRecord($scope.course.resource_url).then(function(cr) {
                $scope.cr = cr;
                $scope.ready = true;
            });
        });
    }]);

app.controller('StudentQuestionsPageController',['$scope', '$state', '$stateParams', 'api', 'db', 'utils',
    function ($scope, $state, $stateParams, api, db, utils) {
        $scope.lessonId = $stateParams.lessonId;
        $scope.input = {};
        $scope.editMode = {};
        $scope.errors = {};
        $scope.studentView = true;
        api.common.getCurrentCourse().then(function(course) {
            $scope.course = course;
            api.student.getLessonRecord($scope.course.resource_url, $scope.lessonId).then(function (record) {
                $scope.lesson = record.lesson;
                $scope.refreshQuestions();
            });
        });
        $scope.refreshQuestions = function() {
            api.student.getLessonQuestionsStudent($scope.lesson.resource_url).then(function (questions) {
                $scope.questions = questions;
                $scope.questions.forEach(function(q) {
                    if (q.mine) {
                        q.highlight = true;
                    }
                    q.answers.forEach(function(a) {
                        if (a.mine) {
                            a.highlight = true;
                        }
                    })
                })
            });
        };
    }]);
