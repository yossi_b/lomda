#!/bin/bash

git pull
python manage.py migrate

python manage.py collectstatic --noinput

if ! diff -q utils/deploy/lomda.conf /etc/apache2/sites-available/lomda.conf > /dev/null ; then
    echo 'Copying...'
    sudo cp utils/deploy/lomda.conf /etc/apache2/sites-available/
    sudo a2ensite lomda

fi

crontab utils/deploy/crontab.txt

sudo service apache2 restart





