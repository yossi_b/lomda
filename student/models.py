from django.core.exceptions import ValidationError
from django.db import models
from django.db import transaction
from django.db.models import Count, Avg
from django.utils import timezone
from django.utils.translation import ugettext as _

from lomda.base_models import SubmittedQuerySet
from utils.utils import avg_no_none


class StudentLesson(models.Model):
    class State:
        ASK = 10
        ANSWER = 20
        EVAL = 30
        DONE = 40
        choices = ((ASK, 'ask'),
                   (ANSWER, 'answer'),
                   (EVAL, 'eval'),
                   (DONE, 'done'))

    student_record = models.ForeignKey('CourseStudentRecord', related_name='lesson_records')
    student = models.ForeignKey('users.Student', related_name='+')
    lesson = models.ForeignKey('core.Lesson', related_name='records')
    state = models.IntegerField(choices=State.choices, default=State.ASK)
    ask_completed = models.BooleanField(default=False)
    answer_completed = models.BooleanField(default=False)
    eval_completed = models.BooleanField(default=False)

    qa_score = models.FloatField(null=True)
    eqa_score = models.FloatField(null=True)

    # we want scores to be hidden till lesson is closed
    def get_qa_score(self):
        if self.lesson.state != self.lesson.State.CLOSED:
            return None
        return self.qa_score

    def get_eqa_score(self):
        if self.lesson.state != self.lesson.State.CLOSED:
            return None
        return self.eqa_score

    def __str__(self):
        return '{0}: {1}'.format(self.student, self.lesson)

    def recompute_scores(self):
        q_scores = list(self.my_questions.values_list('score',flat=True))
        a_scores = list(self.my_answers.values_list('score',flat=True))
        self.qa_score = avg_no_none(*(q_scores+a_scores))
        eq_scores = list(self.evaluated_questions.values_list('score',flat=True))
        ea_scores = list(self.evaluated_answers.values_list('score',flat=True))
        self.eqa_score = avg_no_none(*(ea_scores+eq_scores))
        self.save()
        self.student_record.recompute_scores()

    def save(self, **kwargs):
        self.student = self.student_record.student.user.student
        super().save(**kwargs)

    def set_questions(self):
        qs = self.lesson.questions.annotate(num_answers=Count('answers'))
        qs = qs.exclude(student_lesson=self)
        qs = qs.order_by('num_answers')
        qs = qs[0:3]

        assert self.my_answers.count() == 0, 'already has questions'

        for q in qs:
            self.my_answers.create(question=q)

    @transaction.atomic
    def ask_question(self, *, text):
        self.my_questions.create(text=text, lesson=self.lesson)
        self.set_questions()
        self.state = StudentLesson.State.ANSWER
        self.ask_completed = True
        self.save()

    @transaction.atomic
    def answer_questions(self, answers):
        answers = dict((int(k), v) for k, v in answers.items())
        for a in self.my_answers.all():
            assert not a.submitted
            a.text = answers[int(a.id)]
            a.submitted_at = timezone.now()
            a.submitted = True
            a.save()
        self.state = StudentLesson.State.EVAL
        self.answer_completed = True
        self.save()

    @transaction.atomic
    def eval(self, *, questions, answers):
        assert self.lesson.state == self.lesson.State.EVAL, (
            'lesson {0} is not in EVAL state but in {1} state'.format(self.lesson.id, self.lesson.get_state_display()))
        self._eval(self.evaluated_questions.all(), questions)
        self._eval(self.evaluated_answers.all(), answers)
        self.state = StudentLesson.State.DONE
        self.eval_completed = True
        self.save()
        # recompute the score of each question and answer
        for eq in self.evaluated_questions.select_related('question').all():
            eq.question.recompute_scores()
        for ea in self.evaluated_answers.select_related('answer').all():
            ea.answer.recompute_scores()
        self.recompute_scores()



    def _eval(self, qs, arr):
        for obj in qs:
            assert not obj.submitted
            obj.score = arr[int(obj.id)]
            obj.submitted = True
            obj.submitted_at = timezone.now()
            obj.save()

    class Meta:
        unique_together = (('lesson', 'student_record'),)


class CourseStudentRecord(models.Model):
    course = models.ForeignKey('core.Course', related_name='student_records', verbose_name=_('course'))
    student = models.ForeignKey('users.Student', related_name='course_records', verbose_name=_('student'))
    qa_score = models.FloatField(null=True,blank=True) # ONLY of closed lessons
    eqa_score = models.FloatField(null=True,blank=True) # ONLY of closed lessons

    def get_class_qa_score(self):
        return self.course.class_qa_score

    def recompute_scores(self):
        from core.models import Lesson
        self.qa_score = self.lesson_records.filter(lesson__state=Lesson.State.CLOSED,
                                                   qa_score__isnull=False).aggregate(score=Avg('qa_score'))['score']
        self.eqa_score = self.lesson_records.filter(lesson__state=Lesson.State.CLOSED,
                                                    eqa_score__isnull=False).aggregate(score=Avg('eqa_score'))['score']
        self.save()

    def get_non_initial_lrs(self):
        return self.lesson_records.filter(lesson__state__in=())

    class Meta:
        unique_together = (('course', 'student'),)
        verbose_name = _('student course record')
        verbose_name_plural = _('student course records')

    def clean(self):
        if self.pk:
            orig_student = CourseStudentRecord.objects.get(pk=self.pk).student
            if self.student != orig_student:
                raise ValidationError({'student': '{0}: {1}'.format(_('read only, should be'), orig_student)})
        if self.student.course_records.exclude(course=self.course).count() >= 1:
            raise ValidationError({'student': _('Student can have at most one course for now')})
        super().clean()

    def __str__(self):
        return '{0}'.format(self.student)


class StudentAnswer(models.Model):
    question = models.ForeignKey('core.Question', related_name='answers')
    student_lesson = models.ForeignKey('student.StudentLesson', related_name='my_answers')
    student = models.ForeignKey('users.Student', related_name='+')
    created_at = models.DateTimeField(auto_now_add=True)
    submitted = models.BooleanField(default=False)
    submitted_at = models.DateTimeField(null=True)
    text = models.TextField(null=True)
    teacher_score = models.FloatField(null=True)

    objects = SubmittedQuerySet.as_manager()

    score = models.FloatField(null=True)

    def recompute_scores(self):
        s1 = avg_no_none(*self.evaluations_of_me.values_list('score',flat=True))
        self.score = avg_no_none(s1,self.teacher_score)
        self.save()
        self.student_lesson.recompute_scores()


    def save(self, recompute=False,**kwargs):
        self.student = self.student_lesson.student_record.student.user.student
        super().save(**kwargs)


class EvaluatedAnswer(models.Model):
    answer = models.ForeignKey('StudentAnswer', related_name='evaluations_of_me')
    student_lesson = models.ForeignKey(StudentLesson, related_name='evaluated_answers')
    submitted_at = models.DateTimeField(null=True)
    submitted = models.BooleanField(default=False)
    score = models.IntegerField(default=None, null=True)

    objects = SubmittedQuerySet.as_manager()


class EvaluatedQuestion(models.Model):
    question = models.ForeignKey('core.Question', related_name='evaluations_of_me')
    student_lesson = models.ForeignKey(StudentLesson, related_name='evaluated_questions')
    submitted_at = models.DateTimeField(null=True)
    submitted = models.BooleanField(default=False)
    score = models.IntegerField(default=None, null=True)

    objects = SubmittedQuerySet.as_manager()

    def get_question_id_dict(self):
        return {'id': self.question_id}

