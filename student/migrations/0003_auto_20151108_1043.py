# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('student', '0002_auto_20151103_1811'),
    ]

    operations = [
        migrations.AlterField(
            model_name='studentanswer',
            name='student_lesson',
            field=models.ForeignKey(related_name='my_answers', to='student.StudentLesson'),
        ),
    ]
