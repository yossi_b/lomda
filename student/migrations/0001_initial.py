# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='CourseStudentRecord',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, serialize=False, verbose_name='ID')),
                ('qa_score', models.FloatField(null=True, blank=True)),
                ('eqa_score', models.FloatField(null=True, blank=True)),
            ],
            options={
                'verbose_name_plural': 'רשומות קורס לסטודנט',
                'verbose_name': 'רשומת קורס לסטודנט',
            },
        ),
        migrations.CreateModel(
            name='EvaluatedAnswer',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, serialize=False, verbose_name='ID')),
                ('submitted_at', models.DateTimeField(null=True)),
                ('submitted', models.BooleanField(default=False)),
                ('score', models.IntegerField(null=True, default=None)),
            ],
        ),
        migrations.CreateModel(
            name='EvaluatedQuestion',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, serialize=False, verbose_name='ID')),
                ('submitted_at', models.DateTimeField(null=True)),
                ('submitted', models.BooleanField(default=False)),
                ('score', models.IntegerField(null=True, default=None)),
            ],
        ),
        migrations.CreateModel(
            name='StudentAnswer',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('submitted', models.BooleanField(default=False)),
                ('submitted_at', models.DateTimeField(null=True)),
                ('text', models.TextField(null=True)),
                ('teacher_score', models.FloatField(null=True)),
                ('score', models.FloatField(null=True)),
            ],
        ),
        migrations.CreateModel(
            name='StudentLesson',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, serialize=False, verbose_name='ID')),
                ('state', models.IntegerField(choices=[(10, 'ask'), (20, 'answer'), (30, 'eval'), (40, 'done')], default=10)),
                ('ask_completed', models.BooleanField(default=False)),
                ('answer_completed', models.BooleanField(default=False)),
                ('eval_completed', models.BooleanField(default=False)),
                ('qa_score', models.FloatField(null=True)),
                ('eqa_score', models.FloatField(null=True)),
                ('lesson', models.ForeignKey(to='core.Lesson', related_name='records')),
            ],
        ),
    ]
