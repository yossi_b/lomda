# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0001_initial'),
        ('core', '0003_auto_20151103_1811'),
        ('student', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='studentlesson',
            name='student',
            field=models.ForeignKey(to='users.Student', related_name='+'),
        ),
        migrations.AddField(
            model_name='studentlesson',
            name='student_record',
            field=models.ForeignKey(to='student.CourseStudentRecord', related_name='lesson_records'),
        ),
        migrations.AddField(
            model_name='studentanswer',
            name='question',
            field=models.ForeignKey(to='core.Question', related_name='answers'),
        ),
        migrations.AddField(
            model_name='studentanswer',
            name='student',
            field=models.ForeignKey(to='users.Student', related_name='+'),
        ),
        migrations.AddField(
            model_name='studentanswer',
            name='student_lesson',
            field=models.ForeignKey(to='student.StudentLesson', related_name='answers'),
        ),
        migrations.AddField(
            model_name='evaluatedquestion',
            name='question',
            field=models.ForeignKey(to='core.Question', related_name='evaluations_of_me'),
        ),
        migrations.AddField(
            model_name='evaluatedquestion',
            name='student_lesson',
            field=models.ForeignKey(to='student.StudentLesson', related_name='evaluated_questions'),
        ),
        migrations.AddField(
            model_name='evaluatedanswer',
            name='answer',
            field=models.ForeignKey(to='student.StudentAnswer', related_name='evaluations_of_me'),
        ),
        migrations.AddField(
            model_name='evaluatedanswer',
            name='student_lesson',
            field=models.ForeignKey(to='student.StudentLesson', related_name='evaluated_answers'),
        ),
        migrations.AddField(
            model_name='coursestudentrecord',
            name='course',
            field=models.ForeignKey(related_name='student_records', to='core.Course', verbose_name='קורס'),
        ),
        migrations.AddField(
            model_name='coursestudentrecord',
            name='student',
            field=models.ForeignKey(related_name='course_records', to='users.Student', verbose_name='תלמיד'),
        ),
        migrations.AlterUniqueTogether(
            name='studentlesson',
            unique_together=set([('lesson', 'student_record')]),
        ),
        migrations.AlterUniqueTogether(
            name='coursestudentrecord',
            unique_together=set([('course', 'student')]),
        ),
    ]
