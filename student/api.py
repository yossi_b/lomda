from rest_framework import status
from rest_framework import viewsets, mixins, exceptions
from rest_framework.decorators import list_route, detail_route
from rest_framework.exceptions import ValidationError
from rest_framework.response import Response

from core.models import Lesson
from lomda.api import LomdaMixin
from lomda.permissions import TeacherOnlyPermission, StudentOnlyPermission
from . import serializers


class StudentLessonViewSet(LomdaMixin,
                           mixins.ListModelMixin,
                           mixins.RetrieveModelMixin,
                           viewsets.GenericViewSet):

    permission_classes = [StudentOnlyPermission]

    serializer_class = serializers.StudentLessonSerializerFull

    def get_queryset(self):
        if self.request.user.is_teacher():
            return self.lesson.records.all()

        if self.request.user.is_student():
            return self.lesson.records.filter(student=self.request.user.student)

    def set_record(self):
        object = self.get_object()
        if self.lesson != object.lesson:
            raise exceptions.NotFound('record not in this lesson')
        if self.request.user.is_student() and self.request.user.student != object.student_record.student:
            raise exceptions.PermissionDenied('not your lesson')
        self.record = object

    @list_route(url_path='mine',methods=['GET'])
    def mine(self, request, course_pk, lesson_pk, *args, **kwargs):
        self.set_course(course_pk)
        self.set_lesson(lesson_pk)
        instance = self.get_queryset().get()
        serializer = self.get_serializer(instance)
        return Response(serializer.data)

    @detail_route(methods=['POST'],url_path='ask-question')
    def ask_question(self, request, course_pk, lesson_pk,pk):
        self.set_course(course_pk)
        self.set_lesson(lesson_pk, states=[Lesson.State.ACTIVE])
        self.set_record()
        serializer = serializers.StudentQuestionSerializer(data=request.data,context=self.get_serializer_context())
        serializer.is_valid(raise_exception=True)
        record = self.get_queryset().get()
        if record.state != record.State.ASK:
                raise ValidationError({'non_field_errors':'Wrong state - cannot ask now'})
        self.record.ask_question(text=serializer.validated_data['text'])
        return Response(self.get_serializer(self.record).data, status=status.HTTP_200_OK)

    @detail_route(methods=['POST'],url_path='answer-questions')
    def answer(self, request, course_pk, lesson_pk, pk):
        self.set_course(course_pk)
        self.set_lesson(lesson_pk, states=[Lesson.State.ACTIVE])
        self.set_record()
        serializer = serializers.AnswersCallSerializers(data=request.data)
        serializer.is_valid(raise_exception=True)
        if self.record.state != self.record.State.ANSWER:
            raise ValidationError({'non_field_errors': 'Wrong state - cannot answer now'})
        answers_dict = serializer.validated_data['answers']
        answers_ids = [int(x) for x in answers_dict.keys()]
        if set(answers_ids) != set(self.record.my_answers.values_list('id',flat=True)):
            raise ValidationError({'non_field_errors': 'mismatch of ids'})
        self.record.answer_questions(answers_dict)
        return Response(self.get_serializer(self.record).data, status=status.HTTP_200_OK)

    @detail_route(methods=['POST'],url_path='eval')
    def eval(self, request, course_pk, lesson_pk, pk):
        self.set_course(course_pk)
        self.set_lesson(lesson_pk, states=[Lesson.State.EVAL])
        self.set_record()
        serializer = serializers.EvalCallSerializers(data=request.data)
        serializer.is_valid(raise_exception=True)
        if self.record.state != self.record.State.EVAL:
            raise ValidationError({'non_field_errors': 'Wrong state - cannot eval now'})
        exp_answer_ids = self.record.evaluated_answers.values_list('id',flat=True)
        exp_question_ids = self.record.evaluated_questions.values_list('id',flat=True)
        answer_ids = [int(x) for x in serializer.validated_data['answers'].keys()]
        question_ids = [int(x) for x in serializer.validated_data['questions'].keys()]
        if set(exp_answer_ids) != set(answer_ids):
            raise ValidationError({'non_field_errors': 'mismatch of answer ids'})
        if set(question_ids) != set(exp_question_ids):
            raise ValidationError({'non_field_errors': 'mismatch of question ids'})
        try:
            answers = dict((int(k),int(v)) for k,v in serializer.validated_data['answers'].items())
            questions = dict((int(k),int(v)) for k,v in serializer.validated_data['questions'].items())
        except ValueError as e:
            raise ValueError({'non_field_errors':str(e)})
        self.record.eval(questions=questions, answers=answers)
        return Response(self.get_serializer(self.record).data, status=status.HTTP_200_OK)

    @list_route(methods=['GET'],permission_classes=[TeacherOnlyPermission],url_path='teacher')
    def teacher(self, request, course_pk, lesson_pk, *args, **kwargs):
        self.set_course(course_pk)
        self.set_lesson(lesson_pk)
        queryset = self.filter_queryset(self.get_queryset())
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    @detail_route(methods=['GET'],permission_classes=[TeacherOnlyPermission])
    def user(self, request, course_pk, lesson_pk, pk,*args,**kwargs):
        self.set_course(course_pk)
        self.set_lesson(lesson_pk)
        instance = self.get_object()
        serializer = self.get_serializer(instance)
        return Response(serializer.data)


    def retrieve(self, request, *args, **kwargs):
        raise exceptions.PermissionDenied('not allowed')


