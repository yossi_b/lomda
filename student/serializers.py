from rest_framework import serializers

import core.models
import lomda.serializers
import teacher.serializers
import users.serializers
from . import models


class StudentQuestionSerializer(serializers.ModelSerializer):
    resource_url = lomda.serializers.LomdaUrlField()

    class Meta:
        model = core.models.Question
        fields = ('resource_url',
                  'id',
                  'text',
                  )

    validators = [teacher.serializers.UniqueQuestionTextValidator(), ]


class AnswerSerializer(serializers.ModelSerializer):
    question = StudentQuestionSerializer()
    text = serializers.CharField()
    submitted_at = lomda.serializers.DateTimeUTCField(read_only=True)

    class Meta:
        model = models.StudentAnswer
        fields = ('id',
                  'question',
                  'score',
                  'text',
                  'submitted',
                  'submitted_at')


class AnswersCallSerializers(serializers.Serializer):
    answers = lomda.serializers.LomdaDictField(required=True)


class EvalCallSerializers(serializers.Serializer):
    answers = lomda.serializers.LomdaDictField(required=True)
    questions = lomda.serializers.LomdaDictField(required=True)


class CourseStudentRecordSerializer(serializers.ModelSerializer):
    student = users.serializers.StudentSerializer()

    class Meta:
        model = models.CourseStudentRecord
        fields = ('id',
                  'course',
                  'student',
                  )


class EvaluatedAnswerSerializer(serializers.ModelSerializer):
    text = serializers.CharField(source='answer.text')
    question_id = serializers.IntegerField(source='answer.question_id')

    class Meta:
        model = models.EvaluatedAnswer
        fields = ('id',
                  'text',
                  'question_id')


class EvaluatedQuestionSerializer(serializers.ModelSerializer):
    text = serializers.CharField(source='question.text')
    question_id = serializers.IntegerField(source='question.id')

    class Meta:
        model = models.EvaluatedQuestion
        fields = ('id',
                  'text',
                  'question_id')


class LessonStudentViewSerializer(serializers.ModelSerializer):
    course = core.serializers.CourseSerializer(read_only=True)
    state_end_dt = lomda.serializers.DateTimeUTCField(source='get_state_end_dt')
    state = serializers.CharField(source='get_state_display')
    resource_url = lomda.serializers.LomdaUrlField()


    class Meta:
        model = core.models.Lesson
        fields = ('resource_url',
                  'id',
                  'state',
                  'title',
                  'content',
                  'course',
                  'image',
                  'youtube_id',
                  'state_end_dt')


class StudentLessonSerializerFull(serializers.ModelSerializer):
    state = serializers.CharField(source='get_state_display')
    resource_url = lomda.serializers.LomdaUrlField()
    student = users.serializers.StudentSerializer(source='student_record.student')
    answers = AnswerSerializer(many=True,source='my_answers')
    evaluated_questions = EvaluatedQuestionSerializer(many=True)
    evaluated_answers = EvaluatedAnswerSerializer(many=True)
    lesson = LessonStudentViewSerializer()
    qa_score = serializers.FloatField(source='get_qa_score')
    eqa_score = serializers.FloatField(source='get_eqa_score')

    class Meta:
        model = models.StudentLesson
        fields = ('id',
                  'lesson',
                  'state',
                  'qa_score',
                  'eqa_score',
                  'resource_url',
                  'student',
                  'answers',
                  'evaluated_questions',
                  'evaluated_answers',
                  'eval_completed',
                  'answer_completed',
                  'ask_completed')
