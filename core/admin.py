from django.contrib import admin

import student.models
from . import models


@admin.register(models.Subject)
class SubjectAdmin(admin.ModelAdmin):
    pass

class StudentRecordInline(admin.StackedInline):
    model = student.models.CourseStudentRecord
    extra = 0


@admin.register(models.Course)
class CourseAdmin(admin.ModelAdmin):
    inlines = [
        StudentRecordInline,
    ]

    def get_readonly_fields(self, request, obj):
        if obj:
            return 'teacher', 'subject'
        return []



