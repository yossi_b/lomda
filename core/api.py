from rest_framework import viewsets, exceptions
from rest_framework.decorators import list_route
from rest_framework.response import Response

import core.serializers
import users.logic


class CoursesViewSet(viewsets.GenericViewSet):
    serializer_class = core.serializers.CourseSerializer

    def get_current_course(self):
        course = users.logic.get_current_course(self.request.user)
        if course is None:
            raise exceptions.NotFound('no current course')
        return course

    @list_route(methods=['get'], url_path='current')
    def current(self, request):
        instance = self.get_current_course()
        serializer = self.get_serializer(instance)
        return Response(serializer.data)

