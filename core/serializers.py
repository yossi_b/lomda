from rest_framework import serializers

import core.models
import users.models
import users.serializers
from lomda.serializers import LomdaUrlField


class SubjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = core.models.Subject
        fields = ('id',
                  'title')


class ShortLessonSerializer(serializers.ModelSerializer):
    resource_url = LomdaUrlField()
    state = serializers.CharField(source='get_state_display')

    class Meta:
        model = core.models.Lesson
        fields = ('resource_url',
                  'id',
                  'title',
                  'state')


class CourseSerializer(serializers.ModelSerializer):
    subject = SubjectSerializer(read_only=True)
    teacher = users.serializers.TeacherSerializer(read_only=True)
    current_lesson = ShortLessonSerializer(source='get_current_lesson')
    resource_url = LomdaUrlField()

    class Meta:
        model = core.models.Course
        fields = ('resource_url',
                  'id',
                  'subject',
                  'teacher',
                  'current_lesson')

