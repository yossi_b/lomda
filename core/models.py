import logging

from django.core.exceptions import ValidationError
from django.db import models, transaction
from django.utils import timezone
from django.utils.translation import ugettext as _

import notif.logic
from utils.utils import avg_no_none

LOGGER = logging.getLogger(__name__)


class Subject(models.Model):
    title = models.CharField(_('name'), max_length=200, unique=True)

    class Meta:
        verbose_name = _('Subject')
        verbose_name_plural = _('Subjects')

    def __str__(self):
        return self.title


class Course(models.Model):
    subject = models.ForeignKey(Subject, verbose_name=_('Subject'), related_name='courses')
    teacher = models.ForeignKey('users.Teacher', related_name='courses', verbose_name=_('Teacher'))

    class Meta:
        verbose_name = _('Course')
        verbose_name_plural = _('Courses')
        unique_together = (('subject', 'teacher'),)

    def clean(self):
        if self.teacher.courses.exclude(id=self.id).count() >= 1:
            raise ValidationError({'teacher': _('Teacher can have at most one course for now')})
        super().clean()

    def add_student(self, student):
        self.student_records.create(student=student)

    def get_current_lesson(self):
        try:
            return self.lessons.get(state__in=(Lesson.State.ACTIVE,Lesson.State.EVAL))
        except self.lessons.model.DoesNotExist:
            return None

    @property
    def class_qa_score(self):
        return avg_no_none(*self.student_records.values_list('qa_score',flat=True))


    def __str__(self):
        return '{0} {1} {2}'.format(self.subject, _('by'), self.teacher)


class Lesson(models.Model):
    class State:
        INITIAL = 10
        ACTIVE = 20
        EVAL = 25
        CLOSED = 30
        nexts = {
            INITIAL: ACTIVE,
            ACTIVE: EVAL,
            EVAL: CLOSED,
            CLOSED: None
        }
        start_field = {
            INITIAL: 'created_at',
            ACTIVE: 'active_at',
            EVAL: 'eval_at',
            CLOSED: 'closed_at'
        }
        end_field = {
            INITIAL: None,
            ACTIVE: 'active_end_dt',
            EVAL: 'eval_end_dt',
            CLOSED: None,
        }
        choices = ((INITIAL, 'initial'),
                   (ACTIVE, 'active'),
                   (EVAL, 'eval'),
                   (CLOSED, 'close'))
        trans = [_('initial'),
                 _('active'),
                 _('eval'),
                 _('close')]

    course = models.ForeignKey(Course, related_name='lessons', blank=False, verbose_name=_('course'))
    title = models.CharField(_('title'), max_length=100, blank=False, null=True)
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    active_at = models.DateTimeField(null=True)
    eval_at = models.DateTimeField(null=True)
    closed_at = models.DateTimeField(null=True)
    content = models.TextField(_('content'), null=True)
    state = models.IntegerField(choices=State.choices, default=State.INITIAL)

    active_end_dt = models.DateTimeField(null=True, blank=False)
    eval_end_dt = models.DateTimeField(null=True, blank=False)

    image = models.URLField(null=True)
    youtube_id = models.CharField(max_length=30, null=True)

    def get_state_end_dt(self):
        field = Lesson.State.end_field[self.state]
        if field:
            return getattr(self,field)
        return None


    @property
    def next_state(self):
        return self.State.nexts[self.state]

    @staticmethod
    def get_state_code( display):
        return next(c[0] for c in Lesson.State.choices if c[1] == display)

    def get_next_state_display(self):
        if self.next_state is None:
            return None
        return next(c[1] for c in Lesson.State.choices if c[0] == self.next_state)

    def get_history(self):
        result = []
        for code,state in Lesson.State.choices:
            result.append({
                'state': state,
                'start_dt': getattr(self,Lesson.State.start_field[code]),
                'end_dt': getattr(self,Lesson.State.end_field[code]) if Lesson.State.end_field[code] else None

            })
        return result

    def uncompleted_records(self):
        from student.models import StudentLesson
        if self.state == Lesson.State.ACTIVE:
            return self.records.exclude(state=StudentLesson.State.EVAL)
        if self.state == Lesson.State.EVAL:
            return self.records.exclude(state=StudentLesson.State.DONE)
        assert False,'Lesson {0} should not be in state {1}'.format(self,self.get_state_display())


    def activate(self, active_end_dt, eval_end_dt):
        if self.course.lessons.exclude(id=self.id).filter(state=self.State.ACTIVE).exists():
            raise ValueError(_('There is already an active lesson for this course'))
        if self.course.lessons.exclude(id=self.id).filter(state=self.State.EVAL).exists():
            raise ValueError(_('There is already an evaluated lesson for this course'))
        if self.questions.count() < 3:
            raise ValueError(_('At lease 3 questions are required in order to activate'))
        if active_end_dt is None or eval_end_dt is None:
            raise ValueError(_('Must specify end time for the state and for the eval state'))

        if active_end_dt < timezone.now() or eval_end_dt < timezone.now():
            raise ValueError(_('end time must be in the future'))
        if eval_end_dt < active_end_dt:
            raise ValueError(_('eval end dt must be after active end dt'))
        with transaction.atomic():
            for student_record in self.course.student_records.all():
                student_record.lesson_records.create(lesson=self)
            self.active_at = timezone.now()
            self.state = Lesson.State.ACTIVE
            self.active_end_dt = active_end_dt
            self.eval_end_dt = eval_end_dt
            self.save()
        notif.logic.notify_lesson_new_state(self)

    def evaluate(self, eval_end_dt=None):
        from teacher import logic
        from student.models import StudentLesson
        # force all students into evaluated phase
        with transaction.atomic():
            for r in self.uncompleted_records():
                LOGGER.warning('Student Lesson #%s (%s) was not in EVAL state but in state %s',
                               r.id,
                               r.student,
                               r.get_state_display())
                if r.my_answers.count() == 0:
                    r.set_questions()
                r.state = StudentLesson.State.EVAL
                r.save()

            logic.split_for_eval(self)
            self.state = Lesson.State.EVAL
            self.eval_at = timezone.now()
            if eval_end_dt:
                self.eval_end_dt = eval_end_dt
            self.save()
        notif.logic.notify_lesson_new_state(self)



    def close(self):
        from student.models import StudentLesson
        with transaction.atomic():
            for r in self.uncompleted_records():
                LOGGER.warning('Student Lesson #%s (%s) was not in DONE state but in state %s',
                               r.id,
                               r.student,
                               r.get_state_display())
                r.state = StudentLesson.State.DONE
                r.save()

            self.state = Lesson.State.CLOSED
            self.closed_at = timezone.now()
            self.save()
            for r in self.records.all():
                r.recompute_scores()
        notif.logic.notify_lesson_new_state(self)



    class Meta:
        verbose_name = _('Lesson')
        verbose_name_plural = _('Lessons')
        unique_together = (('course', 'title'),)
        ordering = ('-id',)

    def __str__(self):
        return '{0} {1} {2} {3}'.format(_('lesson'),self.title,_('in'),self.course)


class Question(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    last_edited_at = models.DateTimeField(null=True)
    lesson = models.ForeignKey(Lesson, related_name='questions')
    is_teacher = models.BooleanField(default=False)
    student_lesson = models.ForeignKey('student.StudentLesson',null=True,related_name='my_questions')
    text = models.TextField()
    score = models.FloatField(null=True) # this is the score of the evaluations of the questions!!!
    teacher_score = models.FloatField(null=True) # teacher score for the question

    def get_lesson_dict(self):
        return {
            'id': self.lesson.id,
            'title': self.lesson.title
        }

    def created_by_student(self):
        if self.student_lesson:
            return self.student_lesson.student

    def get_student_user(self):
        return self.student_lesson.student.user if self.student_lesson else None

    @property
    def answers_score(self):
        return avg_no_none(*self.answers.values_list('score',flat=True))

    def recompute_scores(self):
        s1 = avg_no_none(*self.evaluations_of_me.values_list('score',flat=True))
        self.score = avg_no_none(s1,self.teacher_score)
        self.save()
        if self.student_lesson:
            self.student_lesson.recompute_scores()

    def delete_question(self):
        import notif.utils
        with transaction.atomic():
            related_sls = []
            for el in self.evaluations_of_me.select_related('student_lesson'):
                related_sls.append(el.student_lesson)
            if self.student_lesson:
                related_sls.append(self.student_lesson)
                notif.logic.notify_deleted_question(self)
            self.delete()
            for sl in related_sls:
                sl.recompute_scores()


    def __str__(self):
        return self.text

    class Meta:
        unique_together = ('text','lesson'),
        ordering =('created_at', )


