# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0001_initial'),
        ('core', '0002_auto_20151103_1811'),
    ]

    operations = [
        migrations.AddField(
            model_name='course',
            name='teacher',
            field=models.ForeignKey(related_name='courses', to='users.Teacher', verbose_name='מורה'),
        ),
        migrations.AlterUniqueTogether(
            name='question',
            unique_together=set([('text', 'lesson')]),
        ),
        migrations.AlterUniqueTogether(
            name='lesson',
            unique_together=set([('course', 'title')]),
        ),
        migrations.AlterUniqueTogether(
            name='course',
            unique_together=set([('subject', 'teacher')]),
        ),
    ]
