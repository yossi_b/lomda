# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0001_initial'),
        ('student', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='question',
            name='student_lesson',
            field=models.ForeignKey(null=True, related_name='my_questions', to='student.StudentLesson'),
        ),
        migrations.AddField(
            model_name='lesson',
            name='course',
            field=models.ForeignKey(related_name='lessons', to='core.Course', verbose_name='קורס'),
        ),
        migrations.AddField(
            model_name='course',
            name='subject',
            field=models.ForeignKey(related_name='courses', to='core.Subject', verbose_name='נושא'),
        ),
    ]
