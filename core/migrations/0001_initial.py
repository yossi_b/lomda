# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Course',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, serialize=False, verbose_name='ID')),
            ],
            options={
                'verbose_name_plural': 'קורסים',
                'verbose_name': 'קורס',
            },
        ),
        migrations.CreateModel(
            name='Lesson',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(null=True, max_length=100, verbose_name='כותרת')),
                ('created_at', models.DateTimeField(null=True, auto_now_add=True)),
                ('active_at', models.DateTimeField(null=True)),
                ('eval_at', models.DateTimeField(null=True)),
                ('closed_at', models.DateTimeField(null=True)),
                ('content', models.TextField(null=True, verbose_name='תוכן')),
                ('state', models.IntegerField(choices=[(10, 'initial'), (20, 'active'), (25, 'eval'), (30, 'close')], default=10)),
                ('active_end_dt', models.DateTimeField(null=True)),
                ('eval_end_dt', models.DateTimeField(null=True)),
                ('image', models.URLField(null=True)),
                ('youtube_id', models.CharField(null=True, max_length=30)),
            ],
            options={
                'ordering': ('-id',),
                'verbose_name_plural': 'שיעורים',
                'verbose_name': 'שיעור',
            },
        ),
        migrations.CreateModel(
            name='Question',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('last_edited_at', models.DateTimeField(null=True)),
                ('is_teacher', models.BooleanField(default=False)),
                ('text', models.TextField()),
                ('score', models.FloatField(null=True)),
                ('teacher_score', models.FloatField(null=True)),
                ('lesson', models.ForeignKey(to='core.Lesson', related_name='questions')),
            ],
            options={
                'ordering': ('created_at',),
            },
        ),
        migrations.CreateModel(
            name='Subject',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(unique=True, max_length=200, verbose_name='שם')),
            ],
            options={
                'verbose_name_plural': 'נושאים',
                'verbose_name': 'נושא',
            },
        ),
    ]
