��    D      <  a   \      �  6   �               $     ,     3  *   M     x     �  :   �     �     �  +   �                 +   &  1   R  4   �     �     �     �     �     �     �     �     �     �     
  '        7     <     A     D     L     S     l     }     �     �  _   �     "	     '	     <	     A	     E	     c	     x	     ~	     �	     �	     �	     �	     �	     �	     �	     �	     �	     
     
     
     .
     5
     9
     A
     F
     L
  {  Q
  >   �                 
   +  $   6  -   [     �  
   �     �     �  
   �  B   �     %     .     ;  <   D  6   �  8   �     �           	               !  
   *     5  .   E  
   t  I        �     �     �     �  
   �     �     
  %     -   D  
   r     }     �               &  )   -  *   W     �     �  
   �  "   �  $   �     �     �     �               0     B  
   K  -   V     �  
   �     �  
   �     �     �     5   9       6   /   2   *         ;          
   	   %   >         #   A   &   $   <      )       ?                 1                                   7      "          :   D          -          !                  '       ,                (   8   0                     =              3           .      C       B              @      4          +           At lease 3 questions are required in order to activate Core Course Courses Lesson Lesson about to be closed Lesson answer period about to be completed Lessons Lomda Must specify end time for the state and for the eval state Question have been deleted Role Student can have at most one course for now Subject Subjects Teacher Teacher can have at most one course for now There is already an active lesson for this course There is already an evaluated lesson for this course Users active by close content course eight eleven end time must be in the future eval eval end dt must be after active end dt five four in initial lesson lesson is already closed lesson is closed lesson is not in allowed state lesson move to evaluation phase lomda minimal length: {0}, at least {1} digits, at least {2} capital letters and at least {3} letters name new lesson in course nine one password is not strong enough read only, should be seven six student student course record student course records teacher teacher alert ten testings there is already such question thirteen three title title must be unique for course twelve two unknown user users zero Project-Id-Version: 1.0
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-11-08 14:00+0000
PO-Revision-Date: 2015-11-08 14:01-0000
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: he
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 1.5.4
 דרושות שלוש שאלות ע"מ להפעיל שיעור ליבה קורס קורסים שיעור השיעור עומד להסתיים שלב התשובות עומד להסתיים שיעורים לומדה יש לספק זמני סיום שאלה נמחקה תפקיד תלמיד יכול להיות רשום לקורס אחד כרגע נושא נושאים מורה מורה יכול לנהל קורס אחד בלבד כרגע יש שיעור אחר פעיל עבור קורס זה יש שיעור אחר בערכה עבור קורס זה משתמשים פעיל ע"י סיום תוכן קורס שמונה אחד עשרה זמן סיום חייב להיות בעתיד הערכה זמן סיום של ההערכה חייב להיות מאוחר יותר חמש ארבע ב התחלתי שיעור השיעור סגור סיום שיעור השיעור לא במצב הנכון השיעור עובר לשלב ההערכות לומדה אורך מינימלי: {0}, לפחות {1} ספרות, לפחות {2} אותיות גדולות ולפחות {3} אותיות שם שיעור חדש בקורס תשע אחד הסיסמה אינה חזקה מספיק לקריאה בלבד. צריך להיות שבע שש תלמיד רשומת קורס לסטודנט רשומות קורס לסטודנט מורה התראת מורה עשר בדיקות יש כבר שאלה כזאת שלושה עשר שלוש כותרת יש כבר שיעור עם כותרת זאת שתים עשר שתיים לא ידוע משתמש משתמשים אפס 