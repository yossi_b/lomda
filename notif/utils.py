import os.path

from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.mail import EmailMessage
from django.template.loader import render_to_string
from django.utils.translation import ugettext as _


def _normal_user(uid_or_user):
    if isinstance(uid_or_user, int):
        return get_user_model().objects.get(pk=uid_or_user)
    return uid_or_user

STYLES = {
    'good': 'color: green',
    'bad': 'color: red'
}


def send_notifcations(*,users, subject, kind, ctx=None, cc_each=None):
    template = 'notif/{0}.html'.format(kind)
    fake_sent = False
    if cc_each:
        cc_each = [_normal_user(cc) for cc in cc_each]
    if ctx is None:
        ctx = dict()
    for user in users:
        skip = False
        user = _normal_user(user)
        ctx['user'] = user
        ctx['styles'] = STYLES
        message = render_to_string(template,ctx)
        if settings.QA_MODE:
            if user.email.endswith('lomda.com'):
                skip = fake_sent
                fake_sent = True

        if not skip:
            send_email_to_user(user=user,
                               cc_list=cc_each,
                               subject=subject,
                               message=message)



def send_email_to_user(*,user, cc_list, subject, message):
    """
    :param user: user
    :param cc_each: list of cc for this message
    :param subject: of email
    :param message: in html format
    :return: number of messages sent
    sends message to users
    """
    to_list = [user.email]
    cc_list = [cc.email for cc in cc_list] if cc_list else None
    if settings.QA_MODE:
        if user.email.endswith('lomda.com'):
            to_list=settings.FAKE_TO_LIST

    title = ''
    if settings.QA_MODE:
        title = '-{0}'.format(_('testings'))

    if to_list:
        msg = EmailMessage('[{0}{1}] {2}'.format(_('lomda'),
                                                 title,
                                                 subject),
                           message,
                           settings.FROM_EMAIL,
                           to_list,
                           cc=cc_list)
        msg.content_subtype = "html"
        return msg.send()
    return 0


BASE_PATH = '{0}/#'.format(settings.HOST_URL,'#')


def get_link(obj, user):
    return get_lesson_link(obj, user)


def get_lesson_link(lesson, user):
    if user.is_teacher():
        return BASE_PATH + '/teacher/lessons/{0}'.format(lesson.id)
    else:
        return BASE_PATH + '/student/lessons/{0}'.format(lesson.id)

