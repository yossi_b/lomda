import pytz
from django import template
from django.conf import settings
from django.template.defaultfilters import timeuntil

IL_TIMEZONE = pytz.timezone('Asia/Jerusalem')

register = template.Library()


@register.simple_tag
def get_link(obj, user):
    import notif.utils
    return notif.utils.get_link(obj, user)


@register.simple_tag
def base_url():
    return settings.HOST_URL


@register.filter
def lomda_date(dt):
    return dt.astimezone(IL_TIMEZONE).strftime('%d/%m/%Y')


@register.filter
def lomda_hour(dt):
    dt = dt.astimezone(IL_TIMEZONE)
    return dt.strftime('%H:%M')


@register.filter
def lomda_expire_date(dt):
    dt = dt.astimezone(IL_TIMEZONE)
    return '{0} בשעה {1} (עוד {2})'.format(dt.astimezone(IL_TIMEZONE).strftime('%d/%m/%Y'),
                                           dt.astimezone(IL_TIMEZONE).strftime('%H:%M'),
                                           timeuntil(dt))
