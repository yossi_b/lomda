import os

from django.conf import settings
from django.core.mail.backends.base import BaseEmailBackend


class HtmlDebugBackend(BaseEmailBackend):
    def send_messages(self, email_messages):
        assert settings.DEBUG, 'use only in debug mode'
        if not os.path.exists(settings.EMAIL_FILE_PATH):
            os.mkdir(settings.EMAIL_FILE_PATH)

        for msg in email_messages:
            idx = 1
            while True:
                fname = os.path.join(settings.EMAIL_FILE_PATH,'email_{0:05}.html'.format(idx))
                if os.path.exists(fname):
                    idx+=1
                else:
                    break
            with open(fname,'w',encoding='utf-8') as fh:
                fh.write('''
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>{0}</title>
</head>
<body>
{1}
</body>
</html>
                        '''.format(msg.subject,msg.body))
            print('--------------------------------------------------')
            print('To: {0}'.format(','.join(msg.to)))
            if msg.cc:
                print('Cc: {0}'.format(','.join(msg.cc)))
            print('From: {0}'.format(msg.from_email))
            print('Subject: {0}'.format(msg.subject))
            print('Link: file://{0}'.format(fname))
            print('---------------------------------------------------')
            link_name = os.path.join(settings.EMAIL_FILE_PATH,'email_last.html')
            if os.path.exists(link_name):
                os.remove(link_name)
            os.link(fname,link_name)
