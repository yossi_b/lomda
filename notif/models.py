from django.db import models


class NotifLessonOnce(models.Model):
    lesson = models.ForeignKey('core.Lesson', null=True)
    kind = models.CharField(max_length=30, null=True)
    param = models.CharField(max_length=30,null=True)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        unique_together = (('lesson', 'kind', 'param'),)
