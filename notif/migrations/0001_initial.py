# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='NotifLessonOnce',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, serialize=False, verbose_name='ID')),
                ('kind', models.CharField(null=True, max_length=30)),
                ('param', models.CharField(null=True, max_length=30)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('lesson', models.ForeignKey(null=True, to='core.Lesson')),
            ],
        ),
        migrations.AlterUniqueTogether(
            name='notiflessononce',
            unique_together=set([('lesson', 'kind', 'param')]),
        ),
    ]
