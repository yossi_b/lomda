import datetime
import logging

from django.utils import timezone
from django.utils.translation import ugettext as _

from . import utils
from .models import NotifLessonOnce

LOGGER = logging.getLogger(__name__)


def _get_about_to_expire_lessons(state, hours):
    from core.models import Lesson
    kwargs = dict()
    kwargs['{0}__lt'.format(Lesson.State.end_field[state])] = timezone.now() + datetime.timedelta(hours=hours)
    kwargs['state'] = state
    lessons = Lesson.objects.filter(**kwargs)
    return lessons


def notify_teacher_before_end(state, hours):
    from core.models import Lesson
    lessons = _get_about_to_expire_lessons(state, hours)

    kind_name = dict(Lesson.State.choices)[state]
    notif_kind = 'lesson_teacher_end_state_soon'
    notified = NotifLessonOnce.objects.filter(kind=notif_kind, param='{0}:{1}'.format(kind_name,hours)).values_list('lesson_id',flat=True)
    lessons = lessons.exclude(id__in=notified)

    for lesson in lessons:
        #if lesson.uncompleted_records():
        notify_lesson_teacher_before_state_done(lesson, notif_kind, hours)



def notify_before_end_of(state, hours):
    from core.models import Lesson
    lessons = _get_about_to_expire_lessons(state, hours)

    kind_name = dict(Lesson.State.choices)[state]
    notif_kind = 'lesson_end_{0}_soon'.format(kind_name.lower())
    notified = NotifLessonOnce.objects.filter(kind=notif_kind, param=hours).values_list('lesson_id',flat=True)
    lessons = lessons.exclude(id__in=notified)

    for lesson in lessons:
        notify_lesson_before_state_done(lesson, notif_kind, hours)


def notify_lesson_new_state(lesson):
    from core.models import Lesson
    NEW_STATE_NOTIF = {
        Lesson.State.ACTIVE: ('lesson_new', _('new lesson in course')),
        Lesson.State.EVAL: ('lesson_in_eval', _('lesson move to evaluation phase')),
        Lesson.State.CLOSED: ('lesson_closed', _('lesson is closed'))
    }

    uids = list(lesson.records.values_list('student__user_id', flat=True))
    uids.append(lesson.course.teacher.id)
    utils.send_notifcations(users=uids,
                            subject=NEW_STATE_NOTIF[lesson.state][1],
                            kind=NEW_STATE_NOTIF[lesson.state][0],
                            ctx={'lesson': lesson})


def notify_lesson_before_state_done(lesson, notif_kind, hours):
    from core.models import Lesson

    uids = lesson.uncompleted_records().values_list('student__user_id',flat=True)
    LOGGER.info('Sending notification that lesson %s state %s is going to expire in %s hours to user ids %s',
                   lesson, lesson.state, hours, uids)

    if lesson.state == Lesson.State.EVAL:
        subject = _('Lesson about to be closed')
    elif lesson.state == Lesson.State.ACTIVE:
        subject = _('Lesson answer period about to be completed')
    else:
        assert False, '{0} in illegal state {1}'.format(lesson, lesson.get_state_display())

    utils.send_notifcations(users=uids,
                            subject=subject,
                            kind=notif_kind,
                            ctx={'lesson': lesson})

    NotifLessonOnce.objects.create(kind=notif_kind, param=hours, lesson=lesson)


def notify_lesson_teacher_before_state_done(lesson, notif_kind, hours):
    """
     send notification to the teacher, that the lesson is going to be changed
     here we use single template and kind for this
    """
    from core.models import Lesson

    uids = [lesson.course.teacher_id]
    LOGGER.info('Sending notification to the TEACHER that lesson %s state %s is going to expire in %s hours',
                   lesson, lesson.get_state_display(), hours)

    if lesson.state == Lesson.State.EVAL:
        subject = '{0}: {1}'.format(_('teacher alert'),_('Lesson about to be closed'))
    elif lesson.state == Lesson.State.ACTIVE:
        subject = '{0}: {1}'.format(_('teacher alert'),_('Lesson answer period about to be completed'))
    else:
        assert False, '{0} in illegal state {1}'.format(lesson, lesson.get_state_display())

    end_state_field = Lesson.State.end_field[lesson.state]

    utils.send_notifcations(users=uids,
                            subject=subject,
                            kind=notif_kind,
                            ctx={'lesson': lesson,
                                 'end_state_dt':getattr(lesson,end_state_field)})

    NotifLessonOnce.objects.create(kind=notif_kind, param='{0}:{1}'.format(lesson.get_state_display(),hours), lesson=lesson)


def notify_deleted_question(question):
    utils.send_notifcations(users=[question.student_lesson.student.user_id],
                            cc_each=[question.lesson.course.teacher.user_id],
                            subject=_('Question have been deleted'),
                            kind='question_deleted',
                            ctx={'question':question})
