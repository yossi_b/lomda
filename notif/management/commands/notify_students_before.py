import logging

from django.conf import settings
from django.core.management import BaseCommand
from django.utils import translation

import notif.logic
from core.models import Lesson
from utils.decorators import log_err

LOGGER = logging.getLogger(__name__)


class Command(BaseCommand):
    help = 'notify before state change'

    def add_arguments(self, parser):
        parser.add_argument('--hours',type=int,required=True)

    @log_err
    def handle(self, *args, **options):
        translation.activate(settings.LANGUAGE_CODE)
        notif.logic.notify_before_end_of(Lesson.State.ACTIVE,options['hours'])
        notif.logic.notify_before_end_of(Lesson.State.EVAL,options['hours'])


