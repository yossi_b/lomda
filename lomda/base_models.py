from django.db import models


class SubmittedQuerySet(models.QuerySet):
    def submitted(self):
        return self.filter(submitted=True)

