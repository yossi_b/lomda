import pytz
from rest_framework import serializers
from rest_framework.fields import empty
from rest_framework.reverse import reverse

import core.models
import student.models


class LomdaUrlField(serializers.CharField):
    def __init__(self,**kwargs):
        kwargs['read_only'] = True
        kwargs['source'] = '*'
        return super().__init__(**kwargs)

    def to_representation(self, value):
        if isinstance(value,core.models.Lesson):
            return '{0}'.format(reverse('lessons-detail',kwargs={'course_pk':value.course.pk, 'pk':value.pk}))
        if isinstance(value,core.models.Question):
            return '{0}'.format(reverse('questions-detail',kwargs={'course_pk':value.lesson.course.pk,
                                                                  'lesson_pk':value.lesson.pk,
                                                                  'pk':value.pk}))
        if isinstance(value,core.models.Course):
            return reverse('courses-current').replace('current',str(value.pk))
        if isinstance(value,student.models.StudentLesson):
            return reverse('student_lesson-detail',kwargs={
                'course_pk':value.lesson.course.pk,
                'lesson_pk':value.lesson.pk,
                'pk':value.pk})
        assert False,'not yet'


class MineField(serializers.BooleanField):
    def to_representation(self, value):
        user = self.context['view'].request.user
        if isinstance(value,int):
            return user.id == value
        return user == value


class DateTimeUTCField(serializers.DateTimeField):
    """ this forces utc on response
    the problem is that when you save (at least with sqlite, we get the time back in local time zone (as the input)
    and not it utc
    this is not real issue, but mainly for testing
    """
    def to_representation(self, value):
        if value.tzinfo != pytz.utc:
            value = value.astimezone(pytz.utc)
        value = value.replace(microsecond=0)
        value = value.isoformat()
        value = value[:-6] + 'Z'
        return value


class LomdaDictField(serializers.DictField):
    def get_value(self, dictionary):
        return dictionary.get(self.field_name, empty)

    def to_internal_value(self, data):
        import json
        if isinstance(data,str):
            try:
                data = json.loads(data)
            except ValueError:
                pass
        if not isinstance(data, dict):
            self.fail('not_a_dict', input_type=type(data).__name__)
        result = {
            str(key): self.child.run_validation(value)
            for key, value in data.items()
        }
        return result


