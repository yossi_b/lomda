from rest_framework import permissions


class TeacherOnlyPermissionWrite(permissions.IsAuthenticated):
    def has_permission(self, request, view):
        if request.method in permissions.SAFE_METHODS:
            return super().has_permission(request, view) and (request.user.is_teacher() or request.user.is_student())
        else:
            return super().has_permission(request, view) and request.user.is_teacher()


class TeacherOnlyPermission(permissions.IsAuthenticated):
    def has_permission(self, request, view):
        return super().has_permission(request, view) and request.user.is_teacher()


class StudentOnlyPermission(permissions.IsAuthenticated):
    def has_permission(self, request, view):
        return super().has_permission(request, view) and request.user.is_student()
