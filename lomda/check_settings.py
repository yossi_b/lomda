from django.conf import settings
from django.core.checks import Critical, register


@register()
def check_settings(app_configs, **kwargs):
    errors = []
    mode_attrs = ['DEV_MODE','DEPLOY_MODE','QA_MODE']
    for attr in mode_attrs:
        if not hasattr(settings,attr) or not isinstance(getattr(settings,attr),bool):
            errors.append(Critical(
                'Must define {0} of type bool'.format(attr),
                id='LOMDA.E001'
            ))

    vals = [getattr(settings,v) for v in mode_attrs]
    if vals.count(True) != 1:
        errors.append(Critical(
            'One of {0} must be defined as True'.format(mode_attrs),
            id='LOMDA.E001'
        ))

    if not isinstance(settings.FAKE_TO_LIST,list):
        errors.append(Critical(
                'FAKE_TO_LIST msut be list',
                id='LOMDA.E001'
            ))

    return errors
