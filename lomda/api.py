from django.utils.translation import ugettext as _
from rest_framework import exceptions
from rest_framework.generics import get_object_or_404

import core.models


class LomdaMixin(object):
    def set_course(self, course_pk):
        self.course = get_object_or_404(core.models.Course.objects.all(),id=course_pk)
        if self.request.user.is_teacher():
            if self.course.teacher.user != self.request.user:
                raise exceptions.PermissionDenied()
        if self.request.user.is_student():
            if not self.course.student_records.filter(student=self.request.user.student).exists():
                raise exceptions.PermissionDenied()

    def set_lesson(self, lesson_pk, allow_closed=True, states=None):
        self.lesson = get_object_or_404(self.course.lessons.all(),id=lesson_pk)

        if not allow_closed:
            if self.lesson.state == core.models.Lesson.State.CLOSED:
                raise exceptions.ValidationError({'non_field_errors': _('lesson is already closed')})

        if states and self.lesson.state not in states:
            raise exceptions.ValidationError({'non_field_errors': _('lesson is not in allowed state')})

    def _get_param(self,f,*,required):
        val = self.request.query_params.get(f)
        if val is None and required:
            raise exceptions.ValidationError({f:'field is mandatory'})
        return val

    def get_int(self, f, *, required=True):
        val = self._get_param(f,required=required)
        if val is None:
            return None
        try:
            v = int(val)
        except ValueError:
            raise exceptions.ValidationError({f:'field should be numeric'})
        return  v

    def get_char(self, f, *, required=True, choices = None):
        val = self._get_param(f,required=required)
        if val is None:
            return None
        if choices:
            if val not in choices:
                raise exceptions.ValidationError({f:'illgal choice'})
        return val



