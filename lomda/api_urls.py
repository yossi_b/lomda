from rest_framework.routers import DefaultRouter

import core.api
import media.api
import student.api
import teacher.api
import users.api

router = DefaultRouter()
router.register(r'users', users.api.UserViewSet, base_name='users')

router.register(r'media',
                media.api.MediaViewSet,
                base_name='media')

router.register(r'courses', core.api.CoursesViewSet, base_name='courses')
router.register(r'courses/(?P<course_pk>\d+)/lessons',
                teacher.api.LessonViewSet,
                base_name='lessons')

router.register(r'courses/(?P<course_pk>\d+)/lessons/(?P<lesson_pk>\d+)/questions',
                teacher.api.QuestionViewSet,
                base_name='questions')


router.register(r'courses/(?P<course_pk>\d+)/student-records',
                 teacher.api.CourseStudentRecordViewSet,
                 base_name='student-records')

router.register(r'courses/(?P<course_pk>\d+)/questions',
                 teacher.api.CourseQuestionsViewSet,
                 base_name='course-questions')

router.register('courses/(?P<course_pk>\d+)/lessons/(?P<lesson_pk>\d+)/student-records',
                student.api.StudentLessonViewSet,
                base_name='student_lesson')
