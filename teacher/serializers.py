from django.utils.translation import ugettext as _
from rest_framework import serializers, exceptions

import core.models
import core.serializers
import student.models
import users.serializers
from lomda.serializers import LomdaUrlField, MineField, DateTimeUTCField


class UniqueTitleValidator:
    def __call__(self, value):
        title = value.get('title')
        if title is None:
            return
        others = self.view.course.lessons.all()
        if self.pk:
            others = others.exclude(pk=self.pk)
        if others.filter(title=title).exists():
            raise exceptions.ValidationError({'title':_('title must be unique for course')})

    def set_context(self, serializer):
        if serializer.instance and serializer.instance.pk:
            self.pk = serializer.instance.pk
        else:
            self.pk = None
        self.view = serializer.context['view']


class EndDtValidator:
    def __call__(self, value):
        from django.utils import timezone
        eval_end_dt = value.get('eval_end_dt')
        active_end_dt = value.get('active_end_dt')
        if not eval_end_dt and not active_end_dt:
            return
        if self.instance.state in (core.models.Lesson.State.INITIAL, core.models.Lesson.State.CLOSED):
            raise exceptions.ValidationError({'non_field_errors':'Cannot change in this state'})
        if active_end_dt and core.models.Lesson.State == core.models.Lesson.State.EVAL:
            raise exceptions.ValidationError({'active_end_dt':'Cannot change in this state'})

        if eval_end_dt and eval_end_dt < timezone.now():
            raise exceptions.ValidationError({'non_field_errors':_('end time must be in the future')})
        if active_end_dt and active_end_dt < timezone.now():
            raise exceptions.ValidationError({'non_field_errors':_('end time must be in the future')})
        active_after_eval = False
        if active_end_dt and eval_end_dt and active_end_dt > eval_end_dt:
            active_after_eval = True
        if active_end_dt and active_end_dt > self.instance.eval_end_dt:
            active_after_eval = True
        if eval_end_dt and self.instance.active_end_dt > eval_end_dt:
            active_after_eval = True
        if active_after_eval:
            raise exceptions.ValidationError({'non_field_errors':_('eval end dt must be after active end dt')})

    def set_context(self, serializer):
        self.instance = serializer.instance


class UniqueQuestionTextValidator:
    def __call__(self, value):
        text = value.get('text')
        if text is None:
            return
        others = self.view.lesson.questions.all()
        if self.pk:
            others = others.exclude(pk=self.pk)
        if others.filter(text=text).exists():
            raise exceptions.ValidationError({'text':_('there is already such question')})

    def set_context(self, serializer):
        if serializer.instance and serializer.instance.pk:
            self.pk = serializer.instance.pk
        else:
            self.pk = None
        self.view = serializer.context['view']


class TeacherLessonSerializer(serializers.ModelSerializer):
    course = core.serializers.CourseSerializer(read_only=True)
    active_end_dt = DateTimeUTCField()
    eval_end_dt = DateTimeUTCField()
    eval_at = DateTimeUTCField()
    active_at = DateTimeUTCField()
    created_at = DateTimeUTCField(read_only=True)
    state = serializers.CharField(source='get_state_display')
    next_state = serializers.CharField(source='get_next_state_display')
    history = serializers.ListField(source='get_history')
    resource_url = LomdaUrlField()

    class Meta:
        model = core.models.Lesson
        fields = ('resource_url',
                  'id',
                  'state',
                  'next_state',
                  'title',
                  'content',
                  'course',
                  'history',
                  'active_end_dt',
                  'eval_end_dt',
                  'eval_at',
                  'active_at',
                  'created_at',
                  'image',
                  'youtube_id')


class LessonCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = core.models.Lesson
        fields = ('title',
                  'content',
                  'image',
                  'youtube_id')
        extra_kwargs = {
            'title': {'required': True},
            'content': {'required': True}
        }
    validators = [UniqueTitleValidator(), ]


class LessonUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = core.models.Lesson
        fields = ('title',
                  'content',
                  'active_end_dt',
                  'eval_end_dt',
                  'image',
                  'youtube_id')

    validators = [UniqueTitleValidator(), EndDtValidator()]


class LessonDeleteFieldSerializer(serializers.Serializer):
    image = serializers.CharField(required=False)
    youtube_id = serializers.CharField(required=False)


class QuestionSerializer(serializers.ModelSerializer):
    created_by_student = users.serializers.StudentSerializer(read_only=True)
    resource_url = LomdaUrlField()
    created_at = DateTimeUTCField(read_only=True)

    class Meta:
        model = core.models.Question
        fields = ('resource_url',
                  'id',
                  'text',
                  'is_teacher',
                  'created_by_student',
                  'created_at',
                  'last_edited_at'
                  )
        read_only_fields = ('created_by_student', 'created_at', 'id', 'is_teacher', 'last_edited_at')

    validators = [UniqueQuestionTextValidator(), ]


class QuestionEvaluationSerializer(serializers.ModelSerializer):
    student = users.serializers.StudentSerializer(source='student_lesson.student')
    submitted_at = DateTimeUTCField(read_only=True)
    class Meta:
        model = student.models.EvaluatedQuestion
        fields = ('student',
                  'id',
                  'submitted_at',
                  'score',
                  'submitted')


class AnswerEvaluationSerializer(serializers.ModelSerializer):
    student = users.serializers.StudentSerializer(source='student_lesson.student')
    submitted_at = DateTimeUTCField(read_only=True)
    class Meta:
        model = student.models.EvaluatedAnswer
        fields = ('student',
                  'id',
                  'submitted_at',
                  'score',
                  'submitted')


class AnswerFullSerializer(serializers.ModelSerializer):
    student = users.serializers.StudentSerializer(read_only=True)
    evaluations = AnswerEvaluationSerializer(many=True,source='evaluations_of_me',read_only=True)
    class Meta:
        model = student.models.StudentAnswer
        fields = ('id',
                  'score',
                  'text',
                  'student',
                  'submitted',
                  'evaluations',
                  'teacher_score')


class AnswerSerializerStudentView(serializers.ModelSerializer):
    mine = MineField(source='student.user_id')
    class Meta:
        model = student.models.StudentAnswer
        fields = ('id',
                  'text',
                  'submitted',
                  'mine',
                  'score')


class QuestionFullSerializer(serializers.ModelSerializer):
    created_by_student = users.serializers.StudentSerializer(read_only=True)
    evaluations = QuestionEvaluationSerializer(many=True,source='evaluations_of_me',read_only=True)
    answers = AnswerFullSerializer(many=True)
    resource_url = LomdaUrlField()
    lesson = serializers.DictField(source='get_lesson_dict')
    created_at = DateTimeUTCField()
    last_edited_at = DateTimeUTCField()

    class Meta:
        model = core.models.Question
        fields = ('resource_url',
                  'id',
                  'text',
                  'is_teacher',
                  'created_by_student',
                  'created_at',
                  'last_edited_at',
                  'answers',
                  'evaluations',
                  'score',
                  'lesson',
                  'teacher_score',
                  )


class ScoreUpdateSerializer(serializers.Serializer):
    score = serializers.IntegerField(min_value=1,max_value=10,required=True)
    answer_id = serializers.IntegerField(required=False)


class ScoreDeleteSerializer(serializers.Serializer):
    answer_id = serializers.IntegerField(required=False)


class QuestionSerializerStudentView(serializers.ModelSerializer):
    answers = AnswerSerializerStudentView(many=True)
    resource_url = LomdaUrlField()
    lesson = serializers.DictField(source='get_lesson_dict')
    mine = MineField(source='get_student_user')

    class Meta:
        model = core.models.Question
        fields = ('resource_url',
                  'id',
                  'text',
                  'answers',
                  'lesson',
                  'mine',
                  'score'
                  )


class QuestionUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = core.models.Question
        fields = ('text',)

    validators = [UniqueQuestionTextValidator(), ]


class EvalSerializer(serializers.Serializer):
    eval_end_dt = serializers.DateTimeField(required=False)


class ActivateSerializer(serializers.Serializer):
    active_end_dt = serializers.DateTimeField(required=True)
    eval_end_dt = serializers.DateTimeField(required=True)


class StudentLessonSerializer(serializers.ModelSerializer):
    lesson = core.serializers.ShortLessonSerializer()
    state = serializers.CharField(source='get_state_display')
    class Meta:
        model = student.models.StudentLesson
        fields = ('qa_score','eqa_score','lesson','state','id')


class CourseStudentRecordSerializer(serializers.ModelSerializer):
    student = users.serializers.StudentSerializer()
    lesson_records = StudentLessonSerializer(many=True)
    class Meta:
        model = student.models.CourseStudentRecord
        fields = ('id',
                  'course',
                  'student',
                  'qa_score',
                  'eqa_score',
                  'lesson_records'
                  )


class CourseStudentRecordSerializerStudentView(serializers.ModelSerializer):
    student = users.serializers.StudentSerializer()
    lesson_records = StudentLessonSerializer(many=True)
    class_qa_score = serializers.FloatField(source='get_class_qa_score')
    class Meta:
        model = student.models.CourseStudentRecord
        fields = ('id',
                  'course',
                  'student',
                  'qa_score',
                  'eqa_score',
                  'class_qa_score',
                  'lesson_records'
                  )

class CourseStudentRecordSerializerSummaryStudentView(serializers.ModelSerializer):
    student = users.serializers.StudentSerializer()
    class_qa_score = serializers.FloatField(source='get_class_qa_score')
    class Meta:
        model = student.models.CourseStudentRecord
        fields = ('id',
                  'course',
                  'student',
                  'qa_score',
                  'eqa_score',
                  'class_qa_score',
                  )

