

def split_objs_to_uids(objs, uids, non_pred, per_uid_count):
    """
    :param objs: list objects
    :param uids: list of uids
    :param non_pred: func(obj,uid) => returns True if cannot assign to uid
    :param per_uid_count: how much obj each uid should be assigned
    :return: list of tuple(obj, uids)
    """
    class ObjCounter:
        def __init__(self, obj):
            self.obj = obj
            self.uids = []

    counters = [ObjCounter(obj) for obj in objs]

    def assign_to_uid(uid):
        found = 0
        for counter in counters:
            if not non_pred(counter.obj,uid):
                counter.uids.append(uid)
                found+=1
                if found == per_uid_count:
                    return

    for uid in uids:
        counters.sort(key=lambda x:len(x.uids))
        assign_to_uid(uid)
    return [(c.obj, c.uids) for c in counters]


def split_for_eval(lesson):
    questions = list(lesson.questions.all().prefetch_related('answers','answers__student'))
    for q in questions:
        sids = []
        for answer in q.answers.all():
            answer.student_lesson.evaluated_questions.create(question=q)
            sids.append(answer.student_lesson.student_id)
        answers_sids = split_objs_to_uids(q.answers.submitted(), sids, lambda obj, sid: obj.student_id == sid,2)
        for a,asids in answers_sids:
            for asid in asids:
                lesson.records.get(student_id=asid).evaluated_answers.create(answer=a)









