from django.db import transaction
from django.db.models import Q
from django.utils import timezone
from rest_framework import exceptions
from rest_framework import mixins, viewsets, status
from rest_framework.decorators import detail_route, list_route
from rest_framework.exceptions import ValidationError
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response

import core.models
import core.serializers
from lomda.api import LomdaMixin
from lomda.permissions import TeacherOnlyPermission, StudentOnlyPermission
from . import serializers


class LessonViewSet(LomdaMixin,
                    mixins.ListModelMixin,
                    mixins.CreateModelMixin,
                    viewsets.GenericViewSet):
    serializer_class = serializers.TeacherLessonSerializer
    permission_classes = (TeacherOnlyPermission,)

    def get_queryset(self):
        return self.course.lessons.all()

    @list_route(methods=['GET'])
    def active(self, request, course_pk):
        self.set_course(course_pk)
        now = timezone.now()
        instance = get_object_or_404(self.get_queryset(),start_dt__le=now,end_dt__ge=now)
        serializer = self.get_serializer(instance)
        return Response(serializer.data)

    def list(self, request, course_pk, *args, **kwargs):
        self.set_course(course_pk)
        return super().list(course_pk, *args, **kwargs)

    def retrieve(self, request, course_pk, pk):
        self.set_course(course_pk)
        instance = self.get_object()
        serializer = self.get_serializer(instance)
        return Response(serializer.data)

    def create(self, request, course_pk):
        self.set_course(course_pk)
        serializer = serializers.LessonCreateSerializer(data=request.data,context=self.get_serializer_context())
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)

        return Response(self.get_serializer(serializer.instance).data, status=status.HTTP_201_CREATED, headers=headers)

    def perform_create(self, serializer):
        serializer.save(course=self.course)

    @detail_route(methods=['post'])
    def edit(self, request, course_pk, pk):
        self.set_course(course_pk)
        instance = self.get_object()
        self.set_lesson(pk,allow_closed=False)
        serializer = serializers.LessonUpdateSerializer(instance, data=request.data, context=self.get_serializer_context())
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        return Response(self.get_serializer(instance).data)

    def perform_update(self, serializer):
        serializer.save()

    @detail_route(methods=['post'],url_path='delete-field')
    def delete_field(self, request, course_pk, pk):
        self.set_course(course_pk)
        instance = self.get_object()
        self.set_lesson(pk,allow_closed=False)
        serializer = serializers.LessonDeleteFieldSerializer(instance, data=request.data, context=self.get_serializer_context())
        serializer.is_valid(raise_exception=True)
        self.delete_fields(serializer, instance)
        return Response(self.get_serializer(instance).data)

    def delete_fields(self, serializer, instance):
        for k in serializer.validated_data.keys():
            setattr(instance,k,None)
        instance.save()

    @detail_route(methods=['post'])
    def activate(self, request, course_pk, pk):
        self.set_course(course_pk)
        instance = self.get_object()
        self.set_lesson(pk,states=[core.models.Lesson.State.INITIAL])
        serializer = serializers.ActivateSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        try:
            instance.activate(**serializer.validated_data)
        except ValueError as e:
            raise ValidationError({'non_field_errors': str(e)})
        return Response(self.get_serializer(instance).data)

    @detail_route(methods=['post'])
    def evaluate(self, request, course_pk, pk):
        self.set_course(course_pk)
        instance = self.get_object()
        self.set_lesson(pk,states=[core.models.Lesson.State.ACTIVE])
        serializer = serializers.EvalSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        try:
            instance.evaluate(**serializer.validated_data)
        except ValueError as e:
            raise ValidationError({'non_field_errors': str(e)})
        return Response(self.get_serializer(instance).data)

    @detail_route(methods=['post'])
    def close(self, request, course_pk, pk):
        self.set_course(course_pk)
        instance = self.get_object()
        self.set_lesson(pk,states=[core.models.Lesson.State.EVAL])
        try:
            instance.close()
        except ValueError as e:
            raise ValidationError({'non_field_errors': str(e)})
        return Response(self.get_serializer(instance).data)


class CourseStudentRecordViewSet(LomdaMixin, mixins.ListModelMixin, mixins.RetrieveModelMixin, viewsets.GenericViewSet):
    serializer_class = serializers.CourseStudentRecordSerializer
    permission_classes = (TeacherOnlyPermission,)

    def get_queryset(self):
        if self.request.user.is_teacher():
            return self.course.student_records.all()
        else:
            return self.course.student_records.filter(student__user=self.request.user)

    def list(self, request, course_pk, *args, **kwargs):
        self.set_course(course_pk)
        return super().list(request, *args, **kwargs)

    def retrieve(self, request, course_pk, *args, **kwargs):
        self.set_course(course_pk)
        return super().retrieve(request, *args, **kwargs)

    @list_route(methods=['GET'], permission_classes=[StudentOnlyPermission])
    def mine(self, request, course_pk, *args, **kwargs):
        self.set_course(course_pk)
        instance = get_object_or_404(self.get_queryset())
        serializer = serializers.CourseStudentRecordSerializerStudentView(instance,context=self.get_serializer_context())
        return Response(serializer.data)

    @list_route(methods=['GET'], permission_classes=[StudentOnlyPermission],url_path='mine/summary')
    def mine_summary(self, request, course_pk, *args, **kwargs):
        self.set_course(course_pk)
        instance = get_object_or_404(self.get_queryset())
        serializer = serializers.CourseStudentRecordSerializerSummaryStudentView(instance,context=self.get_serializer_context())
        return Response(serializer.data)


class QuestionViewSet(mixins.CreateModelMixin,
                      mixins.ListModelMixin,
                      mixins.DestroyModelMixin,
                      mixins.UpdateModelMixin,
                      LomdaMixin,
                      viewsets.GenericViewSet):
    serializer_class = serializers.QuestionSerializer
    permission_classes = (TeacherOnlyPermission,)

    def create(self, request, course_pk, lesson_pk):
        self.set_course(course_pk)
        self.set_lesson(lesson_pk, allow_closed=False)
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)

        return Response(self.get_serializer(serializer.instance).data, status=status.HTTP_201_CREATED, headers=headers)

    def perform_create(self, serializer):
        serializer.save(is_teacher=True, lesson=self.lesson)


    def get_queryset(self):
        return self.lesson.questions.all()

    def list(self, request, course_pk, lesson_pk, *args, **kwargs):
        self.set_course(course_pk)
        self.set_lesson(lesson_pk)
        return super().list(request, *args, **kwargs)

    @list_route(methods=['GET'],permission_classes=[StudentOnlyPermission],url_path='student-view')
    def student_view(self, request, course_pk, lesson_pk, *args, **kwargs):
        self.set_course(course_pk)
        self.set_lesson(lesson_pk)
        queryset = self.filter_queryset(self.get_queryset())
        serializer = serializers.QuestionSerializerStudentView(queryset,many=True,context=self.get_serializer_context())
        return Response(serializer.data)


    @list_route(methods=['GET'])
    def full(self, request, course_pk, lesson_pk, *args, **kwargs):
        self.set_course(course_pk)
        self.set_lesson(lesson_pk)

        queryset = self.filter_queryset(self.get_queryset())
        sl_id = self.get_int('student_lesson_id',required=False)
        kind = self.get_char('kind',choices=['qa','eqa'],required=False)

        if sl_id and not kind or kind and not sl_id:
            raise exceptions.ValidationError({'non_field_errors':'must specify kind and student_lesson_id together'})
        if sl_id:
            sl =get_object_or_404( self.lesson.records,id=sl_id)
            if kind == 'qa':
                queryset = queryset.filter(Q(student_lesson_id=sl) |
                                           Q(answers__student_lesson=sl)).distinct()
            else:
                queryset = queryset.filter(Q(evaluations_of_me__student_lesson_id=sl)|
                                           Q(answers__evaluations_of_me__student_lesson=sl)).distinct()


        serializer = serializers.QuestionFullSerializer(queryset,many=True)
        return Response(serializer.data)


    def destroy(self, request, course_pk, lesson_pk, *args, **kwargs):
        self.set_course(course_pk)
        self.set_lesson(lesson_pk)
        instance = self.get_object()
        with transaction.atomic():
            instance.delete_question()
        return Response(status=status.HTTP_204_NO_CONTENT)



    @detail_route(methods=['post'])
    def edit(self, request, course_pk, lesson_pk, pk):
        self.set_course(course_pk)
        self.set_lesson(lesson_pk, allow_closed=False)
        instance = self.get_object()
        serializer = serializers.QuestionUpdateSerializer(instance, data=request.data,context=self.get_serializer_context())
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        return Response(self.get_serializer(instance).data)

    def perform_update(self, serializer):
        serializer.save(last_edited_at=timezone.now())

    def _save_delete_score(self,answer_id, score):
        instance = self.get_object()
        # score may be None - deletion
        with transaction.atomic():
            if answer_id:
                answer = get_object_or_404(instance.answers,pk=answer_id)
                answer.teacher_score = score
                answer.save()
                answer.recompute_scores()
            else:
                instance.teacher_score = score
                instance.save()
                instance.recompute_scores()
        serializer2 = serializers.QuestionFullSerializer(instance)
        return Response(serializer2.data)

    @detail_route(methods=['post'],url_path='set-teacher-score')
    def set_teacher_score(self, request, course_pk, lesson_pk, pk):
        self.set_course(course_pk)
        self.set_lesson(lesson_pk, allow_closed=True)
        serializer = serializers.ScoreUpdateSerializer(data=request.data,context=self.get_serializer_context())
        serializer.is_valid(raise_exception=True)
        data = serializer.validated_data
        score = data['score']
        answer_id = data.get('answer_id')
        return self._save_delete_score(answer_id,score)

    @detail_route(methods=['post'],url_path='delete-teacher-score')
    def delete_teacher_score(self, request, course_pk, lesson_pk, pk):
        self.set_course(course_pk)
        self.set_lesson(lesson_pk, allow_closed=True)
        instance = self.get_object()
        serializer = serializers.ScoreDeleteSerializer(instance, data=request.data,context=self.get_serializer_context())
        serializer.is_valid(raise_exception=True)
        data = serializer.validated_data
        answer_id = data.get('answer_id')
        return self._save_delete_score(answer_id,None)

class CourseQuestionsViewSet(LomdaMixin,
                             mixins.ListModelMixin,
                             viewsets.GenericViewSet):
    serializer_class = serializers.QuestionFullSerializer
    permission_classes = (TeacherOnlyPermission,)

    @list_route(methods=['GET'])
    def full(self, request, course_pk, *args, **kwargs):
        self.set_course(course_pk)
        kind = self.get_char('kind')
        cr_id = self.get_int('cr_id')
        cr = get_object_or_404(self.course.student_records,pk=cr_id)
        queryset = core.models.Question.objects.filter(lesson__course=self.course).order_by('lesson__id','id')
        if kind == 'qa':
            queryset = queryset.filter(Q(student_lesson__student_record=cr) |
                                       Q(answers__student_lesson__student_record=cr)).distinct()
        elif kind == 'eqa':
            queryset = queryset.filter(Q(evaluations_of_me__student_lesson__student_record=cr)|
                                       Q(answers__evaluations_of_me__student_lesson__student_record=cr)).distinct()

        serializer = serializers.QuestionFullSerializer(queryset, many=True)
        return Response(serializer.data)



