from rest_framework import serializers
from . import models


class TeacherSerializer(serializers.ModelSerializer):
    name = serializers.CharField(source='user.name')
    id = serializers.IntegerField(source='user.id')

    class Meta:
        model = models.Teacher
        fields = ('name','id')


class StudentSerializer(serializers.ModelSerializer):
    name = serializers.CharField(source='user.name')
    id = serializers.IntegerField(source='user.id')

    class Meta:
        model = models.Student
        fields = ('name','id')


class LoginSerializer(serializers.Serializer):
    email = serializers.EmailField()
    password = serializers.CharField()


class SelfSerializer(serializers.ModelSerializer):
    token = serializers.CharField(source='auth_token.key')
    role = serializers.CharField(source='get_role_display')
    class Meta:
        model = models.User
        fields = ('id', 'name', 'email', 'role', 'token', 'is_student', 'is_teacher')
