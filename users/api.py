from rest_framework import status
from rest_framework import viewsets
from rest_framework.authtoken.models import Token
from rest_framework.decorators import list_route
from rest_framework.response import Response
from rest_framework import permissions
from rest_framework import exceptions
from users.serializers import TeacherSerializer, StudentSerializer, LoginSerializer
from django.contrib.auth import authenticate

from .consts import UserRole
from . import serializers


class UserViewSet(viewsets.GenericViewSet):
    serializer_class = serializers.SelfSerializer

    @list_route(url_path='login',methods=['post'], permission_classes=(permissions.AllowAny,))
    def login(self, request):
        ls = LoginSerializer(data=request.data)
        if not ls.is_valid():
            return Response(ls.errors, status=status.HTTP_400_BAD_REQUEST)
        user = authenticate(**ls.data)
        if user is None:
            raise exceptions.AuthenticationFailed('invalid login')
        if user.role == UserRole.UNKNOWN:
            raise exceptions.PermissionDenied('must be teacher/user')
        Token.objects.get_or_create(user=user)
        return Response(self.get_serializer(user).data)

    @list_route(url_path='logout',methods=['post'])
    def logout(self, request):
        request.user.auth_token.delete()
        return Response(status=204)






