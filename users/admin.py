from authtools.admin import StrippedNamedUserAdmin
from authtools.forms import UserCreationForm
from django.contrib import admin
from django.contrib.auth.forms import AdminPasswordChangeForm
from django.utils.translation import ugettext as _

import users.logic

from . import models

class StrongPasswordMixin():
    def clean_password2(self):
        password2 = super().clean_password2()
        if not users.logic.password_ok(password2):
            self.add_error('password2','{0}:{1}'.format(_('password is not strong enough'),
                                                        users.logic.get_password_desc()))
        return password2

class ValidatedPasswordAdminPasswordChangeForm(StrongPasswordMixin, AdminPasswordChangeForm):
    pass

class ValidatedPasswordUserCreationForm(StrongPasswordMixin, UserCreationForm):
    pass

@admin.register(models.User)
class UserAdmin(StrippedNamedUserAdmin):
    list_display = ('id', 'role') + StrippedNamedUserAdmin.list_display
    fieldsets = list(StrippedNamedUserAdmin.fieldsets)

    add_form = ValidatedPasswordUserCreationForm
    change_password_form = ValidatedPasswordAdminPasswordChangeForm
    def get_readonly_fields(self, request, obj=None):
        if obj:
            return ['role',]
        return []



