from django.contrib.auth import get_user_model
from django.core.checks import Error, register
from django.db import transaction
from django.utils.translation import ugettext as _

from . import models
from .consts import UserRole


@transaction.atomic
def create_student(email, name, password):
    if not password_ok(password):
        raise ValueError('Illegal password')
    user = get_user_model().objects.create_user(email=email, role=UserRole.STUDENT, name=name, password=password)
    return user.student


@transaction.atomic
def create_teacher(email, name, password):
    if not password_ok(password):
        raise ValueError('Illegal password')
    user = get_user_model().objects.create_user(email=email, role=UserRole.TEACHER, name=name, password=password)
    return user.teacher

"""
@register()
def check_settings(app_configs, **kwargs):
    errors = []
    for t in models.Teacher.objects.all().select_related('user'):
        if t.id != t.user.id:
            errors.append(Error('Inconsistency in ids for teacher {0}'.format(t.id)))

    for s in models.Student.objects.all().select_related('user'):
        if s.id != s.user.id:
            errors.append(Error('Inconsistency in ids for student {0}'.format(s.id)))

    return errors
"""

def get_current_course(user):
    from core.models import Course
    from student.models import CourseStudentRecord
    try:
        if user.is_student():
            return user.student.course_records.get().course
        elif user.is_teacher():
            return user.teacher.courses.get()
    except (Course.DoesNotExist, CourseStudentRecord.DoesNotExist):
        pass
    return None


def get_password_desc():
    return _(
        'minimal length: {0}, at least {1} digits, at least {2} capital letters and at least {3} letters').format(6, 1,
                                                                                                                 1, 3)

def password_ok(password):
    if len(password) < 6:
        return False
    digit_count = len([c for c in password if c.isdigit()])
    upper_count = len([c for c in password if c.isupper()])
    alpah_count = len([c for c in password if c.isalpha()])
    if digit_count < 1:
        return False
    if upper_count < 1:
        return False
    if alpah_count < 3:
        return False
    return True
