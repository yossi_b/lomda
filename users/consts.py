from django.utils.translation import ugettext_lazy as _


class UserRole:
    TEACHER = 10
    STUDENT = 20
    UNKNOWN = 30

    codes = {TEACHER: 'teacher',
             STUDENT: 'student',
             UNKNOWN: 'unknown'}

    choices = ((TEACHER,_('teacher')),
               (STUDENT,_('student')),
               (UNKNOWN,_('unknown')))

