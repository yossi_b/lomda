from authtools.models import AbstractNamedUser
from django.db import models
from django.db import transaction
from django.utils.translation import ugettext_lazy as _

from . import consts


class User(AbstractNamedUser):
    REQUIRED_FIELDS = ['name', 'role']
    role = models.IntegerField(_('Role'), choices=consts.UserRole.choices, default=consts.UserRole.UNKNOWN)

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')

    def is_student(self):
        return self.role == consts.UserRole.STUDENT

    def is_teacher(self):
        return self.role == consts.UserRole.TEACHER

    def get_role_code(self):
        return consts.UserRole.codes.get(self.role, 'unknown')

    def __str__(self):
        return self.name

    def save(self,*args,**kwargs):
        with transaction.atomic():
            new = not self.pk
            super().save(*args, **kwargs)
            if new:
                if self.role == consts.UserRole.STUDENT:
                    Student.objects.create(user=self)

                if self.role == consts.UserRole.TEACHER:
                    Teacher.objects.create(user=self)




class Student(models.Model):
    user = models.OneToOneField(User)

    def save(self,*args,**kwargs):
        if not self.pk:
            self.id = self.user.id
        return super().save(*args,**kwargs)

    def __str__(self):
        return str(self.user)


class Teacher(models.Model):
    user = models.OneToOneField(User)

    def save(self,*args,**kwargs):
        if not self.pk:
            self.id = self.user.id
        return super().save(*args,**kwargs)


    def __str__(self):
        return str(self.user)

